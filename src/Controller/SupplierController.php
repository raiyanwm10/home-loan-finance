<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

class SupplierController extends AppController
{

    public $connection;
    public $supplier_infoTable;

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent("Paginator");
        $this->loadComponent('Flash');
        $this->connection = ConnectionManager::get("default");
        $this->supplier_infoTable = TableRegistry::getTableLocator()->get("supplier_info");
    }

    //supplier/orm-insert-data
    public function ormInsertData(){
        $this->autoRender=false;
        $query = $this->supplier_infoTable->query();
        $data = $query->insert(["ContactName","CompanyName","PhoneNo","Email","Address"])
            ->values(["ContactName"=>"Noobkid" ,
                "CompanyName"=>"Noobcompany",
                "PhoneNo"=>"09876890",
                "Email"=>"noobkid@gmail.com",
                "Address"=>"Noob world"
            ])->execute();

        print_r($data);
    }

    //supplier/orm-update-data
    public function ormUpdateData(){
        $this->autoRender = false;
        $query = $this->supplier_infoTable->query();
        $data = $query->update()->set(
            ["ContactName"=>"Cleverkid"
            ,"CompanyName"=>"CleverKIDs Company",
                "Email"=>"cleverkid@gmail.com",])
        ->where(["SupplierID"=>4])
        ->execute();
        print_r($data);
    }

    //supplier/orm-delete-data
    public function ormDeleteData(){
        $this->autoRender = false;
        $query = $this->supplier_infoTable->query();
        $data = $query->delete()->where(["SupplierID"=>4])
        ->execute();

        print_r($data);
    }

    //insert data in table


    //supplier/insert-data
    public function insertData(){
        $this->autoRender = false;
        $data=$this->connection->insert("supplier_info",array ("ContactName"=>"Pikachu" ,"CompanyName"=>"Pokemon","PhoneNo"=>"09876890","Email"=>"pikachu@gmail.com","Address"=>"Ash's Shoulder"));
        echo "<pre>";
        print_r($data);
    }

    //supplier/update-data
    public function updateData(){
        $this->autoRender = false;
        $this->connection->update("supplier_info",["ContactName"=>"Raichu","Email"=>"raichu@gmail.com"],["SupplierID"=>3]);
    }

    //supplier/delete-data
    public function deleteData(){
        $this->autoRender = false;
        $this->connection->delete("supplier_info",
        ["SupplierID"=>3]);

        echo "<h3>Row has been deleted</h3>";
    }

    //load-connection-details
    public function loadConnectionDetails(){
        $this->autoRender=false;
        echo "<pres>";
        print_r($this->connection);
    }



   public function index(){
        $this -> viewBuilder()->setLayout("custom/supplierinfoworking");
        $supplier = $this->Paginator->paginate($this->supplier_infoTable->find());

       $this->set("suppliers",$supplier);

       $supplier_info = $this->supplier_infoTable->newEmptyEntity();

       if ($this->request->is("post")){
           $supplier_info = $this->supplier_infoTable->patchEntity($supplier_info,$this->request->getData());

           $supplier_info->SupplierID = "";

           if ($this->supplier_infoTable->save($supplier_info)){
               $this->Flash->success("Successfully Saved");
               return $this->redirect(["action" => "index"]);
           }
           $this->Flash->error("Unable to save");
       }

       $this->set("supplierss",$supplier_info);

   }
   public function edit($SupplierID){
       $supplier_infoy = $this->supplier_infoTable->get($SupplierID);

       if ($this->request->is(["post","put"])){

           $supplier_infoy = $this->supplier_infoTable->patchEntity($supplier_infoy,$this->request->getData());

           $supplier_infoy->modified = date("Y-m-d H:i:s");
           $this->supplier_infoTable->save($supplier_infoy);
           return $this->redirect(["action"=>"index"]);

       }
       $this->set("ContactName",$supplier_infoy->ContactName);
       $this->set("ContactName",$supplier_infoy->CompanyName);
       $this->set("Email",$supplier_infoy->Email);
       $this->set("PhoneNo",$supplier_infoy->PhoneNo);
       $this->set("Address",$supplier_infoy->Address);
       $this->set("suppliers",$supplier_infoy);

   }

   public function delete($SupplierID){
        $this->request->allowMethod(["post","delete"]);
        $supplier_infoz =$this->supplier_infoTable->get($SupplierID);
        $this->supplier_infoTable->delete($supplier_infoz);
        return $this->redirect(["action"=>"index"]);
   }

}
