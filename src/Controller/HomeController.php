<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Mailer;

/**
 * Enquiries Controller
 *
 * @property \App\Model\Table\EnquiriesTable $Enquiries
 * @method \App\Model\Entity\Enquiry[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */

class HomeController extends AppController
{
    public $connection;
    public $contact_infoTable;
    public $message;
    public $logoTable;
    public $footerTable;
    public $creditguideTable;
    public $emailTable;

    public function initialize(): void
    {

        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->connection = ConnectionManager::get('default');
        $this->contact_infoTable = TableRegistry::getTableLocator()->get('contact_info');
        $this->message = TableRegistry::getTableLocator()->get('message');
        $this->logoTable = TableRegistry::getTableLocator()->get('logo');
        $this->footerTable = TableRegistry::getTableLocator()->get('footer');
        $this->creditguideTable = TableRegistry::getTableLocator()->get('creditguide');
        $this->emailTable = TableRegistry::getTableLocator()->get('email');

    }
    public function loginPage()
    {
        $this -> viewBuilder()->setLayout('custom/loginpage');
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
    }

    public function logout()
    {
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            $this->Authentication->logout();

            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }
    public function landingpage()
    {
        $this -> viewBuilder()->setLayout('custom/landingpage');
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }
    public function services()
    {
        $this -> viewBuilder()->setLayout('custom/services');
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }
    public function calculatorone()
    {
        $this -> viewBuilder()->setLayout('custom/calculatorone');
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }
    public function confirmed()
    {
        $this -> viewBuilder()->setLayout('custom/confirmed');
        $query = $this->message
            ->find() //or this
            ->order(['created' => 'DESC']);

        $inventory = $this->Paginator->paginate($query, ['limit' => '1']);

        $this->set('messages', $inventory);
    }
    public function contact()
    {
        $this -> viewBuilder()->setLayout('custom/contact');

        $query = $this->contact_infoTable
            ->find('all'); //or this

        $inventory = $this->Paginator->paginate($query, ['limit' => '100']);

        $this->set('inventorys', $inventory);
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);

        $enquiry = $this->message->newEmptyEntity();
        if ($this->request->is('post')) {
            $enquiry = $this->message->patchEntity($enquiry, $this->request->getData());
            $footer = $this->footerTable->get(1);
            $contactxy = $this->contact_infoTable->get(1);
            $emailformat = $this->emailTable->get(1);
            if ($this->message->save($enquiry)) {
                // Send email
                $mailer = new Mailer('default');
                $mailerx = new Mailer('default');
                // Setup email parameters
                $mailer
                    ->setEmailFormat('html')
                    ->setTo(Configure::read('EnquiryMail.to'))
                    ->setFrom(Configure::read('EnquiryMail.from'), 'Home Loan Finance')
                    ->setReplyTo($enquiry->email)
                    ->setSubject('New message from ' . h($enquiry->first_name) . " | " . h($enquiry->subject))
                    ->viewBuilder()
                    ->disableAutoLayout()
                    ->setTemplate('enquiry');
                
                $mailerx
                    ->setEmailFormat('html')
                    ->setTo($enquiry->email)
                    ->setFrom(Configure::read('EnquiryMail.from'), 'Home Loan Finance')
                    ->setSubject( 'Hi '.h($enquiry->first_name).', We have received your mail and will get back to you soon ')
                    ->setAttachments([
                        'Credit Guide.pdf' => [
                            'file' => 'webroot/files/Credit Guide.pdf',
                            'mimetype' => 'application/pdf',
                            'contentId' => 'my-unique-id'
                        ]
                    ])
                    ->viewBuilder()
                    ->disableAutoLayout()
                    ->setTemplate('confirmation');

                // Send data to the email template
                $mailer->setViewVars([
                    'content' => $enquiry->body,
                    'first_name' => $enquiry->first_name,
                    'last_name' => $enquiry->last_name,
                    'phone_no' => $enquiry->phone_no,
                    'email' => $enquiry->email,
                    'subject' => $enquiry->subject,
                    'created' => $enquiry->created,
                    'id' => $enquiry->id,
                    'copyright' =>$footer->copyright,
                    'abn' =>$footer->abn,
                    'acr' =>$footer->acr,
                    'acl' =>$footer->acl,
                    'afca' =>$footer->afca,
                    'mfaa' =>$footer->mfaa
                ]);
                
                 $mailerx->setViewVars([
                    'first_name' => $enquiry->first_name,
                    'address' =>$contactxy->address,
                    'phone' =>$contactxy->phone,
                    'copyright' =>$footer->copyright,
                    'abn' =>$footer->abn,
                    'acr' =>$footer->acr,
                    'acl' =>$footer->acl,
                    'afca' =>$footer->afca,
                    'mfaa' =>$footer->mfaa,
                    'bodytext1' => $emailformat->bodytext1,
                    'bodytext2' => $emailformat->bodytext2,
                    'bodytext3' => $emailformat->bodytext3,
                    'regardstext1' => $emailformat->regardstext1,
                    'regardstext2' => $emailformat->regardstext2
                ]);
                
                //Send email
                $email_result = $mailer->deliver();
                $mailerx->deliver();

                if ($email_result) {
                    $enquiry->email_sent = ($email_result) ? true : false;
                    $this->message->save($enquiry);
                    $this->Flash->success(__('The enquiry has been saved and sent via email.'));
                } else {
                    $this->Flash->error(__('Email failed to send. Please check the enquiry in the system later. '));
                }

                return $this->redirect(['action' => 'confirmed']);
            }
            $this->Flash->error(__('The enquiry could not be saved. Please, try again.'));
        }
        $this->set(compact('enquiry'));

    }
    public function editcontact()
    {
        $this -> viewBuilder()->setLayout('custom/editcontact');
        $contact_info = $this->contact_infoTable->get(1);
        if ($this->request->is(['post', 'put'])) {
            $contact_info = $this->contact_infoTable->patchEntity($contact_info, $this->request->getData());

            $this->contact_infoTable->save($contact_info);
            $this->set('email', $contact_info->email);
            $this->set('emailc', $contact_info->emailc);
            $this->set('address', $contact_info->address);
            $this->set('phone', $contact_info->phone);
            
            if ($this->contact_infoTable->save($contact_info)) {
                $this->Flash->success(__('Contact page updated'),['key' => 'contactupdate']);
            } else {
                $this->Flash->error("Failed to update contact page");
            }
            
        }
            
        $this->set('contact_info', $contact_info);
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);

    }
    
    public function editfooter()
    {
        $this -> viewBuilder()->setLayout('custom/editfooter');

        $footer = $this->footerTable->get(1);
        if ($this->request->is(['post', 'put'])) {
            $footer = $this->footerTable->patchEntity($footer, $this->request->getData());

            $this->footerTable->save($footer);
            $this->set('email', $footer->email);
            $this->set('phone', $footer->phone);
            $this->set('address', $footer->address);
            $this->set('copyright', $footer->copyright);
            $this->set('abn', $footer->abn);
            $this->set('acr', $footer->acr);
            $this->set('acl', $footer->acl);
            $this->set('afca', $footer->afca);
            $this->set('mfaa', $footer->mfaa);
            
            if ($this->footerTable->save($footer)) {
                $this->Flash->success(__('Footer updated'),['key' => 'footerupdate']);
            } else {
                $this->Flash->error("Failed to update footer");
            }
            
            $this->set('footer', $footer);
        }
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);

    }
    
    public function editemail()
    {
        $this -> viewBuilder()->setLayout('custom/editemail');
        $emailformat = $this->emailTable->get(1);
        if ($this->request->is(['post', 'put'])) {
            $emailformat = $this->emailTable->patchEntity($emailformat, $this->request->getData());

            $this->emailTable->save($emailformat);
            $this->set('bodytext1', $emailformat->bodytext1);
            $this->set('bodytext2', $emailformat->bodytext2);
            $this->set('bodytext3', $emailformat->bodytext3);
            $this->set('regardstext1', $emailformat->regardstext1);
            $this->set('regardstext2', $emailformat->regardstext2);
            
        
        if ($this->emailTable->save($emailformat)) {
                $this->Flash->success(__('Email format updated'),['key' => 'emailupdate']);
            } else {
                $this->Flash->error("Failed to update email format");
            }
        }
        
        $this->set('emailformat', $emailformat);
        
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
        
        $query = $this->contact_infoTable
            ->find('all'); //or this

        $inventory = $this->Paginator->paginate($query, ['limit' => '100']);

        $this->set('inventorys', $inventory);

    }

    public function editlogo()
    {
        $this -> viewBuilder()->setLayout('custom/editlogo');
        $product = $this->logoTable->get(90);

        if ($this->request->is(['post', 'put'])) {

            $data = $this->request->getData();
            $productImage = $this->request->getData("image");

            $hasFileError = $productImage->getError();

            if ($hasFileError > 0) {
                // no file uploaded
                $data["image"] = "";
            } else {
                // file uploaded
                $fileName = $productImage->getClientFilename();
                $fileType = $productImage->getClientMediaType();

                if ($fileType == "image/png" || $fileType == "image/jpeg" || $fileType == "image/jpg") {
                    $imagePath = WWW_ROOT . "logos/" . $fileName;
                    $productImage->moveTo($imagePath);
                    $data["image"] = "logos/" . $fileName;
                }
            }

            $product = $this->logoTable->patchEntity($product, $data);

            if ($this->logoTable->save($product)) {
                $this->Flash->success(__('Logo updated'),['key' => 'logoupdate']);
            } else {
                $this->Flash->error("Failed to upload image");
            }
        }

        $this->set(compact("product"));
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }
    public function editcreditguide()
    {
        $this -> viewBuilder()->setLayout('custom/editcreditguide');
        $productsx = $this->creditguideTable->get(1);

        if ($this->request->is(['post', 'put'])) {

            $data = $this->request->getData();
            $productImage = $this->request->getData("filepdf");

            $hasFileError = $productImage->getError();

            if ($hasFileError > 0) {
                // no file uploaded
                $data["filepdf"] = "";
            } else {
                // file uploaded
                $fileName = $productImage->getClientFilename();
                $fileType = $productImage->getClientMediaType();

                if ($fileType == "application/pdf") {
                    $imagePath = WWW_ROOT . "files/" . $fileName;
                    $productImage->moveTo($imagePath);
                    $data["filepdf"] = "files/" . $fileName;
                    rename (WWW_ROOT . "files/" . $fileName, WWW_ROOT . "files/" . "Credit Guide.pdf");
                }
            }

            $productsx = $this->creditguideTable->patchEntity($productsx, $data);

            if ($this->creditguideTable->save($productsx)) {
                $this->Flash->success(__('Credit Guide updated'),['key' => 'creditupdate']);
            } else {
                $this->Flash->error("Failed to upload file");
            }
        }

        $this->set(compact("productsx"));
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }

    public function contactmsgpage()
    {
        $this -> viewBuilder()->setLayout('custom/contactmsgpage');

        $inventorykey = $this->request->getQuery('msgkey');
        if ($inventorykey) {
            $query = $this->message->find('all')->where(['Or' => ['first_name like' => '%' . $inventorykey . '%','last_name like' => '%' . $inventorykey . '%','email like' => '%' . $inventorykey . '%']]);
        } else {
            $query = $this->message
                ->find() //or this
                ->order(['created' => 'DESC']);
        }

        $inventory = $this->Paginator->paginate($query, ['limit' => '100']);

        $this->set('messages', $inventory);
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }
    
    public function export()
    {
        $data = $this->message
            ->find()
            ->select(['first_name','last_name','email','phone_no']);

        $header = ['FirstName','LastName','EmailAddress','WorkPhone'];
        $this->setResponse($this->getResponse()->withDownload('Opportunity.csv'));
        $this->set(compact('data'));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOptions([
                'serialize' => 'data',
                'header' => $header
            ]);
    }
    
    public function deletemsg($id)
    {
        $this->request->allowMethod(['post','delete']);
        $inventory_infoz = $this->message->get($id);
        $this->message->delete($inventory_infoz);

        return $this->redirect(['action' => 'contactmsgpage']);
    }
    public function deletemsgb($id)
    {
        $this->request->allowMethod(['post','delete']);
        $inventory_infoz = $this->message->get($id);
        $this->message->delete($inventory_infoz);

        return $this->redirect(['action' => 'dashboardPage']);
    }
    public function messageview($id)
    {
        $this -> viewBuilder()->setLayout('custom/messageview');
        $msg_infox = $this->message->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('msg_infox'));
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }
    public function msgread($id)
    {
        $queryObject = $this->message->query();

        $queryObject->update()->set([
            "msgstatus"=>"Working"
        ])
            ->where([
                "id" => $id
            ])
            ->execute();


        return $this->redirect(['action' => 'contactmsgpage']);

    }
    public function msgsolved($id)
    {
        $queryObject = $this->message->query();

        $queryObject->update()->set([
            "msgstatus"=>"Solved"
        ])
            ->where([
                "id" => $id
            ])
            ->execute();


        return $this->redirect(['action' => 'contactmsgpage']);

    }
    public function settingspage()
    {
        $this -> viewBuilder()->setLayout('custom/settingspage');
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }

    public function dashboardPage()
    {
        $this -> viewBuilder()->setLayout('custom/dashboardpage');


        $query = $this->message
            ->find() //or this
            ->order(['created' => 'DESC']);

        $inventory = $this->Paginator->paginate($query, ['limit' => '5']);

        $this->set('messages', $inventory);
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }
}
