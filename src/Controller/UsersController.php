<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
use Cake\Mailer\Mailer;
use Cake\Mailer\TransportFactory;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public $UsersTable;
    public $footerTable;
    public $logoTable;

    public function initialize(): void
    {

        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->connection = ConnectionManager::get('default');
        $this->footerTable = TableRegistry::getTableLocator()->get('footer');
        $this->logoTable = TableRegistry::getTableLocator()->get('logo');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
    }

    public function login()
    {
        $this->viewBuilder()->setLayout("custom/loginpage");
        

        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();
        // debug($result);
        // exit;
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            // redirect to /articles after login success
            $redirect = $this->request->getQuery('redirect', [
                'controller' => 'home',
                'action' => 'dashboardPage',
            ]);

            return $this->redirect($redirect);
        }
        // display error if user submitted and authentication failed
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'),['key' => 'InvalidPassword']);
        }
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
    }

    public function register()
    {

        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            
            
            $mytoken = Security::hash(Security::randomBytes(32));

            
            $user->token = $mytoken;

            if ($this->Users->save($user)) {
                $this->Flash->success((__("Your registration was successful.")));

                return $this->redirect('/');
            }

            $this->Flash->error(__("Your registration failed."));
        }
    }

    public function forgotpassword()
    {
        $this->viewBuilder()->setLayout("custom/forgotpassword");
        
        if ($this->request->is('post')) {
            $email = $this->request->getData('email');
            $footer = $this->footerTable->get(1);
            $token = Security::hash(Security::randomBytes(25));

            
            if ($email == NULL) {
                $this->Flash->error(__('Please insert your email address'));
            }
            if ($user = $this->Users->find('all')->where(['email' => $email])->first()) {
                $user->token = $token;
                if ($this->Users->save($user)) {
                    $mailer = new Mailer('default');
                    $mailer
                    ->setFrom(Configure::read('EnquiryMail.from'), 'Home Loan Finance')
                        ->setTo($email)
                        ->setEmailFormat('html')
                        ->setSubject('Forgot Password Request')
                        ->viewBuilder()
                        ->disableAutoLayout()
                        ->setTemplate('resetpassword');
                }
                $mailer->setViewVars([
                    'token' => $user->token,
                    'copyright' =>$footer->copyright,
                    'abn' =>$footer->abn,
                    'acr' =>$footer->acr,
                    'acl' =>$footer->acl,
                    'afca' =>$footer->afca,
                    'mfaa' =>$footer->mfaa
                ]);
                $mailer->deliver();
                $this->Flash->success('Reset password link has been sent to your email (' . $email . '), please check your email');
                $this->redirect(['controller'=>'users','action' => 'resetpasswordrequest']);
            }
            if ($total = $this->Users->find('all')->where(['email' => $email])->count() == 0) {
                $this->Flash->error(__('Email is not registered in system'),['key' => 'InvalidEmail']);
            }
        }
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
    }

    public function resetpassword($token)
    {
         $this->viewBuilder()->setLayout("custom/resetpassword");
        if ($this->request->is('post')) {
            
            $mypass = $this->request->getData('password');


            $user = $this->Users->find('all')->where(['token' => $token])->first();
            $user->password = $mypass;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Password successfully reset. Please login using your new password'),['key' => 'passwordresetsuccess']);
                return $this->redirect(['controller'=>'users','action' => 'resetpasswordcomplete']);
            }
        }
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
    }
    
    public function resetpasswordrequest()
    {
        $this->viewBuilder()->setLayout("custom/resetpasswordrequest");

    }
    public function resetpasswordcomplete()
    {
        $this->viewBuilder()->setLayout("custom/resetpasswordcomplete");

    }

    // public function logout()
    // {
    //     $result = $this->Authentication->getResult();
    //     // regardless of POST or GET, redirect if user is logged in
    //     if ($result->isValid()) {
    //         $this->Authentication->logout();
    //         return $this->redirect(['controller' => 'Users', 'action' => 'login']);
    //     }
    // }



    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->viewBuilder()->setLayout("custom/userspage");

        $userskey = $this->request->getQuery('userskey');
        if ($userskey) {
            $query = $this->Users->find('all')->where(['Or' => ['email like' => '%' . $userskey . '%', 'role like' => '%' . $userskey . '%']]);
        } else {
            $query = $this->Users;
        }

        $users = $this->Paginate($query, ['limit' => '100']);

        $this->set('users', $users);
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout("custom/usersviewpage");
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout("custom/registerpage");
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                // $this->Flash->success(__('The user has been saved.'));

                // $redirect = $this->request->getQuery('redirect', [
                //     'controller' => 'home',
                //     'action' => 'dashboardPage',
                // ]);

                // return $this->redirect($redirect);

                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function adduser()
    {
        $this->viewBuilder()->setLayout("custom/adduser");
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            
            $mytoken = Security::hash(Security::randomBytes(32));
            $user->token = $mytoken;
            
            if ($this->Users->save($user)) {
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout("custom/editusers");
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        
        $queryx = $this->footerTable
            ->find('all'); //or this

        $footerx = $this->Paginator->paginate($queryx, ['limit' => '100']);

        $this->set('footerx', $footerx);
        
        $logox = $this->logoTable
            ->find('all'); //or this

        $logox = $this->Paginator->paginate($logox, ['limit' => '100']);

        $this->set('logox', $logox);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
