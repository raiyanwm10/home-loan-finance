<?php

namespace App\Model\Entity;
use \Cake\ORM\Entity;

class supplier_infoEntity extends Entity
{
    protected $_accessible = [
        "ContactName"=> true,
        "CompanyName"=> true,
        "PhoneNo"=> true,
        "Email"=> true,
        "Address"=> true
    ];
}
