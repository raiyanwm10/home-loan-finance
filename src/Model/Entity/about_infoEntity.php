<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class about_infoEntity extends Entity
{
    protected $_accessible = [
        "about"=> true
    ];
}
