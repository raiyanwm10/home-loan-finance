<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class order_infoEntity extends Entity
{
    protected $_accessible = [
        "CustomerFirstName"=> true,
        "CustomerLastName"=> true,
        "CustomerEmail"=> true,
        "CustomerAddress"=> true,
        "CustomerCity"=> true,
        "CustomerState"=> true,
        "CustomerPostCode"=> true,
        "products"=> true,
        "paidamount"=> true,
        'created' => true,
        'ProductStatus'=> true,
    ];
}
