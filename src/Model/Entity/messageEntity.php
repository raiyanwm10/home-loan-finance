<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class messageEntity extends Entity
{
    protected $_accessible = [
        "emailaddress"=> true,
        "messagetxt"=> true,
        "contactname"=> true,
        "msgstatus"=> true,
    ];
}
