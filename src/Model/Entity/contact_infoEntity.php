<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class contact_infoEntity extends Entity
{
    protected $_accessible = [
        "phone"=> true,
        "email"=> true,
        "address"=> true,
        "message"=> true
    ];
}
