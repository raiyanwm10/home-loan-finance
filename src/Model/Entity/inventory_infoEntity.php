<?php

namespace App\Model\Entity;
use \Cake\ORM\Entity;

class inventory_infoEntity extends Entity
{
    protected $_accessible = [
        "ProductName"=> true,
        "ProductDescription"=> true,
        "ProductCost"=> true,
        "ProductStock"=> true
    ];
}
