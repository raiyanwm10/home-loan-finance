// Listen for submit

document.getElementById("loan-form").addEventListener("submit", computeResults);

function computeResults(e) {
  // UI

  const UIamount = document.getElementById("amount").value;
  const UIinterest = document.getElementById("interest").value;
  const UIyears = document.getElementById("years").value;

  // Calculate

  const principal = parseFloat(UIamount);
  const CalculateInterest = parseFloat(UIinterest) / 100 / 12; //r
  const calculatedPayments = parseFloat(UIyears) * 12;         //n

  const CalculateInterestweekly = parseFloat(UIinterest) / 100 / 52;
  const calculatedPaymentsweekly = parseFloat(UIyears) * 52;

  const CalculateInterestfortnightly = parseFloat(UIinterest) / 100 / 26;
  const calculatedPaymentsfortnightly = parseFloat(UIyears) * 26;

  //Compute monthly Payment

  const x = Math.pow(1 + CalculateInterest, calculatedPayments);
  const monthly = (principal * x * CalculateInterest) / (x - 1);
  const monthlyPayment = monthly.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
  });


  //Compute fortnightly Payment

  const y = Math.pow(1 + CalculateInterestfortnightly, calculatedPaymentsfortnightly);
  const fortnightly = (principal * y * CalculateInterestfortnightly) / (y - 1);
  const fortnightlyPayment = fortnightly.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
  });

  //Compute weekly Payment

  const z = Math.pow(1 + CalculateInterestweekly, calculatedPaymentsweekly);
  const weekly = (principal * z * CalculateInterestweekly) / (z - 1);
  const weeklyPayment = weekly.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
  });

  //Compute Interest

  const totalInterest = (monthly * calculatedPayments - principal).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
  });
  const totalInterestmonthlywithprincipal = (monthly * calculatedPayments).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
  });

  //Compute Interest Fortnightly

  const totalInterestfortnightly = (fortnightly * calculatedPaymentsfortnightly - principal).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
  });
  const totalInterestfortnightlywithprincipal = (fortnightly * calculatedPaymentsfortnightly).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
  });

  //Compute Interest Weekly

  const totalInterestweekly = (weekly * calculatedPaymentsweekly - principal).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
  });
  const totalInterestweeklywithprincipal = (weekly * calculatedPaymentsweekly).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
  });



  document.getElementById("monthlyPayment").innerHTML = "$" + monthlyPayment;

  document.getElementById("fortnightlyPayment").innerHTML = "$" + fortnightlyPayment;

  document.getElementById("weeklyPayment").innerHTML = "$" + weeklyPayment;

  document.getElementById("totalInterest").innerHTML = "$" + totalInterest;

  document.getElementById("totalInterestfortnightly").innerHTML = "$" + totalInterestfortnightly;

  document.getElementById("totalInterestweekly").innerHTML = "$" + totalInterestweekly;

  document.getElementById("totalInterestmonthlywithprincipal").innerHTML = "$" + totalInterestmonthlywithprincipal;

  document.getElementById("totalInterestfortnightlywithprincipal").innerHTML = "$" + totalInterestfortnightlywithprincipal;

  document.getElementById("totalInterestweeklywithprincipal").innerHTML = "$" + totalInterestweeklywithprincipal;

  e.preventDefault();
}
