<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <title>Home Loan Finance | 404 Error</title>
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
</head>
<style>
    *{
        margin: 0%;
        padding: 0%;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
    }

    .hero{
        width: 100%;
        height: 100vh;
        background-image: linear-gradient(rgba(255, 255, 255, 0.0),rgba(255, 255, 255, 0.0));
        position: relative;
        padding: 0 5%;
        display: flex;
        align-items: center;
    }
    nav{
        width: 100%;
        position: fixed;
        top: 0;
        left: 0;
        padding: 10px 8%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        transition: 0.6s;
        font-family: 'Poppins', sans-serif;
    }

    nav .logo{
        position: relative;
        width: 130px;
        transition: 0.6s;
    }

    nav ul li {
        list-style: none;
        display: inline-block;
        margin-left: 40px;
        transition: 0.6s;
    }

    nav ul li a{
        text-decoration: none;
        color: #fff;
        font-size: 17px;
        transition: 0.6s;
    }
    .nav-item::after{
        content:'';
        display: block;
        width: 0;
        height: 2px;
        background: #ffffff;
        transition: width .3s;
    }
    .nav-item:hover::after{
        width:100%;
        transition: width .3s;
    }
    .nav-itema::after{
        content:'';
        display: block;
        height: 2px;
        background: #ffffff;
        width:100%;
    }
    .nav-menu{
        padding-top: 25px;
    }

    .hamburger{
        display: none;
        cursor: pointer;
    }

    .bar{
        display: block;
        width: 25px;
        height: 3px;
        margin: 5px auto;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background-color: #FFFFFF;
    }
    video{
        width: 100%;
        position: absolute;
        right: 0%;
        bottom: 0%;
        z-index: -1;
    }
    @media(min-aspect-ratio: 16/9){
        video{
            width: 100%;
            height: auto;
        }
    }
    @media(max-aspect-ratio: 16/9){
        video{
            width: auto;
            height: 100%;
        }
    }
    @media(max-width:850px){
        .hamburger{
            display: block;
        }

        .hamburger.active .bar:nth-child(2){
            opacity: 0;
        }
        .hamburger.active .bar:nth-child(1){
            transform: translateY(8px) rotate(45deg);
        }
        .hamburger.active .bar:nth-child(3){
            transform: translateY(-8px) rotate(-45deg);
        }
        .nav-menu{
            padding-top: 150px;
            position: fixed;
            right: -100%;
            top: 0px;
            gap: 0;
            flex-direction: column;
            background: rgb(149,177,210);
            background: linear-gradient(200deg, rgba(65,94,125,1) 0%, rgba(48,61,88,1) 100%);
            width: 100%;
            text-align: left;
            transition: 0.3s;
            height: 100%;

        }
        .nav-item{
            display: list-item;
            margin: 20px 40px;
        }

        .nav-menu.active{
            right: 0;
            width: 60%;
            height: 100%;
        }
        .nav-item::after{
            content:'';
            display: none;
            width: 0;
            height: 2px;
            background: #ffffff;
            transition: width .3s;
        }
        .nav-item:hover::after{
            width:100%;
            transition: width .3s;
        }
    }
    .content{
        text-align: left;
        padding: 10px 4%;
    }
    .content h1{
        font-size: 74px;
        color: #fff;
        font-weight: 900;
        transition: 0.5s;
    }
    .content h1:hover{
        -webkit-text-stroke: 1px #fff;
        color: transparent;
    }
    .content h2{
        font-size: 27.35px;
        color: #fff;
        font-weight: 600;
    }
    .button-area{
        margin-top: 30px;
    }
    .appointment-button a {
        font-weight: 600;
        border-radius: 5px;
        text-decoration: none;
        padding: 10px 5% 10px 5%;
        color: rgb(255, 255, 255);
        transition: 0.5s;
        border-style:solid;
        border-width: 3px;
        border-color: #FFFFFF;
    }
    .appointment-button a:hover {
        border-color: #ffffff;
        border-style:solid;
        border-width: 3px;
        background-color:rgb(255, 255, 255);
        color: rgb(0, 0, 0);
    }
    .primary-footer {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 50px;
    }
    hr{
        width: 100%;
    }
    .footer-layout p{
        font-size: 12px;
        color: #FFFFFF;
    }
    .footer-layout{
        margin: auto;
        width: 83.75%;
    }
    .footer-items{
        display: flex;
        justify-content: space-between;
    }
    .copyright2{
        display: none;
    }
    .footer-items a{
        text-decoration: none;
        color: #fff;
        font-size: 12px;
    }
    .left-section{
        text-align: left;
        color: #fff;
        font-size: 12px;
        margin-top: 5px;
    }
    .right-section{
        margin-top: 5px;
        text-align: right;
        min-width: 240px;
    }
    .nav-itemp{
        display: none;
    }
    .nav-itemx p{
        display: none;
    }
    @media(max-width:850px){
        .nav-itemp{
            text-align: left;
            display: block;
        }
        .nav-itemx p{
            display: block;
            margin-top: 20px;
            color: #FFFFFF;
            font-size: 10px;
        }
        .nav-itemp p{
            margin-top: 5px;
            font-size: 12px;
        }
        .hamburger-footer{
            margin-top: 85%;
        }
        .primary-footer{
            display: none;
        }
    }
    @media(max-width:1010px){
        .copyright2{
            display: block;
        }
        .emailandphone{
            display: none;
        }
        .address1{
            display: none;
        }
        .copyright1{
            display: none;
        }
    }

</style>
<body>

<header class="primary-header">
    <div class="hero">

        <?= $this->Html->media(
            [['src' => '/webroot/video/silkerror1.mp4', 'type' => "video/mp4"]],
            ['autoplay','loop','muted','plays-inline']
        ) ?>


        <div class="primary-footer">
            <div class="footer-layout">
                
                <div>
                </div>
            </div>
        </div>


        <div class="content">
            <div class="headline-text">
                <h1>ERROR 404</h1>
                <h2>You reached the end of the world<br>Time to get back home</h2>
            </div>
            <div class="button-area">
                <div class="appointment-button"><a <?php echo $this->Html->link('<span class="ITEMX1">GO BACK TO HOME</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["controller"=>"home","action"=>"landingpage"],
                        ['escape' => false, 'title' => __('')]) ?> </a></div>
            </div>

        </div>



    </div>


</header>


</body>

</html>
