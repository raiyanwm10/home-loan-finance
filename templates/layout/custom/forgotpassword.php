<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />
    <title>Home Loan Finance | Forgot Password</title>

    <style>
        * {
            margin: 0%;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Poppins', sans-serif;

        }

        .buttona input[type=submit]:hover {
            background: #5676de;
            color: white;
        }
        .buttona input[type=submit]{
            color: white;
        }
        button.buttona{
            border: none;
        }

        .buttona input[type=submit] {
            cursor: pointer;
            background: rgb(129, 161, 220);
            box-shadow: 0 3px 5px rgba(0, 0, 0, 0.3);

            padding-top: 10px;
            padding-bottom: 10px;
            padding-left: 25px;
            padding-right: 25px;
            border-radius: 5px;
            transition: .4s;

        }

        a.goback {
            margin-left: 20px;
            background-color: black;

        }

        .allbuttons {
            text-align: center;
            margin-top: 15px;
            width: 100%;

        }

        .buttona input {
            cursor: pointer;
            background: #ffffff;
            font-size: 15px;
            border: none;

        }


        span.buttonb {
            cursor: pointer;
            background: #ffffff;
            box-shadow: 0 3px 5px rgba(0, 0, 0, 0.3);
            padding-top: 10px;
            padding-bottom: 10px;
            padding-left: 15px;
            padding-right: 15px;
            border-radius: 5px;
            cursor: pointer;
            transition: .4s;

        }

        span.buttonb:hover {
            cursor: pointer;
            background: #80c1ff;
            box-shadow: 0 3px 5px rgba(0, 0, 0, 0.3);
            padding-top: 10px;
            padding-bottom: 10px;
            padding-left: 15px;
            padding-right: 15px;
            border-radius: 5px;
            cursor: pointer;
        }

        .allbuttons a {
            text-decoration: none;
        }

        a:link {
            color: #000000;
        }

        /* visited link */
        a:visited {
            color: #000000;
        }

        /* mouse over link */
        a:hover {
            color: #000000;
        }

        /* selected link */
        a:active {
            color: #000000;
        }

        .login {
            text-align: center;
        }

        .login h3 {
            font-size: 30px;
        }
        .message.error{
            margin-top: 10px;
            text-align: center;
            cursor: pointer;
        }
        #togglePassword{
            cursor: pointer;
        }


        input[type=text],
        select,
        textarea {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
            font-family: 'Poppins', sans-serif;
        }
        input[type=email],
        select,
        textarea {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
            font-family: 'Poppins', sans-serif;
        }
        .bi-eye-slash{
            margin-left: 5px;
        }
        #togglePassword.bi.bi-eye-slash::before{
            margin-bottom: 2px;
        }

        input[type=password],
        select,
        textarea {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
            font-family: 'Poppins', sans-serif;
        }

        .primary-footer {
            background: rgb(132, 153, 178);
            position: absolute;
            left: 0px;
            bottom: 0px;
            width: 100%;
            height: 110px;
            margin-top: 10px;
        }
        hr{
        width: 100%;
        background-color: white;
        }
        .footer-layout p{
            font-size: 12px;
            color: #FFFFFF;
        }
    .footer-layout{
        margin: 35px auto;
        width: 83.75%;
    }
    .footer-items{
        display: flex;
        justify-content: space-between;
    }
    .footer-items a{
        text-decoration: none;
        color: #fff;
        font-size: 12px;
    }
    .left-section{
        text-align: left;
        color: #fff;
        font-size: 12px;
        margin-top: 5px;
    }
    .right-section{
        margin-top: 5px;
        text-align: right;
        min-width: 240px;
    }
        .loginpage{
            width: 100%;
            position: absolute;
            align-items: center;
        }

        .List_Group_Desktop {
            position: relative;
            width: 100%;
            background-color: #ffffff;
            box-shadow: black;
            align-items: center;
        }

        .users_form {
            margin-top:10px;
            filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
            position: relative;
        }
        .login-body{
            margin: 150px auto;
            padding: 2% 4%;
            height: 500px;
            width: 90%;
            max-width: 550px;
            background-color: #ffffff;
            border-radius: 10px;
            box-shadow: black
        }
        .logoimg{
            width: 250px;
        }
        .flashmsg{
            text-align: center;
            color: darkred;
            width: 100%;
        }
        h3{
            margin-top: 10px;
        }

        @media(max-width:850px){
            footer{
                display: none;
            }
            .login-body{
                height: 500px;
                width: 100%;
                max-width: 400px;
            }
        }


    </style>

</head>

<body>

<div class="loginpage">



    <div class="List_Group_Desktop">

        <div class="login-body">
            <div class="login">
                <img src="<?php echo APPLICATION_URL ?>/webroot/logos/Logowide2.png" class="logoimg">
                <h3>Forgot Password</h3>
                <h4>Please enter your email to reset password</h4>
            </div>
            <div class="users_form">

                <?php echo $this->Flash->render() ?>
                <?= $this->Form->create() ?>

                <?= $this->Form->control('email', ["maxlength"=>"34",'required' => true,'placeholder'=>'e.g. youremail@gmail.com']) ?>


                <div class="flashmsg"> <?= $this->Flash->render('InvalidEmail') ?></div>
            </div>
            <div class="allbuttons">

                <button class="buttona"> <?= $this->Form->submit(__('Submit')); ?></button>
                <?= $this->Form->end() ?>
            </div>


        </div>
    </div>

</div>
<footer>
    <div class="primary-footer">
        <div class="footer-layout">
            <hr>
            <?php
            foreach ($footerx as $footer){?>
            <div class="footer-items">
                <div class="left-section">
                    <p class="copyright2"><?=$footer->copyright?></p>
                </div>
                <div class="center-section">

                </div>
                <div class="right-section">
                    <p class="security-no">ABN: <?=$footer->abn?> | ACR: <?=$footer->acr?> | ACL: <?=$footer->acl?> | AFCA: <?=$footer->afca?> | MFAA: <?=$footer->mfaa?></p>
                </div>
            </div>
            <?php
            }
            ?>
            <div>
            </div>
        </div>
    </div>
</footer>


</body>

</html>

