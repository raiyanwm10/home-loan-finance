<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <title>Home Loan Finance | Mail Inbox</title>
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
</head>
<style>
    *{
        margin: 0%;
        padding: 0%;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
    }
    ::-webkit-scrollbar{
        width: 10px;
    }
    ::-webkit-scrollbar-thumb{
        background: #86a8fd;
        border-radius: 10px;
    }

    ::-webkit-scrollbar-thumb:hover  {
        background: #86a8fd;

    }

    .hero{
        width: 100%;
        height: 5vh;
        background-image: linear-gradient(rgba(255, 255, 255, 0.0),rgba(255, 255, 255, 0.0));
        position: relative;
        display: flex;
        align-items: center;
        background-color: rgb(255, 255, 255);
    }
    nav{
        width: 100%;
        background: rgb(149,177,210);
        background: linear-gradient(90deg, rgba(149,177,210,1) 0%, rgba(65,94,125,1) 100%);
        position: fixed;
        top: 0;
        left: 0;
        padding: 10px 4%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        transition: 0.6s;
        font-family: 'Poppins', sans-serif;
        z-index: +1;
    }

    nav .logo{
        position: relative;
        width: 130px;
        transition: 0.6s;
    }

    nav ul li {
        list-style: none;
        display: inline-block;
        margin-left: 40px;
        transition: 0.6s;
    }

    nav ul li a{
        text-decoration: none;
        color: #fff;
        font-size: 17px;
        transition: 0.6s;
    }
    .nav-item{
        display: none;
    }
    .nav-item::after{
        content:'';
        display: block;
        width: 0;
        height: 2px;
        background: #ffffff;
        transition: width .3s;
    }
    .nav-item:hover::after{
        width:100%;
        transition: width .3s;
    }
    .nav-itema::after{
        content:'';
        display: block;
        height: 2px;
        background: #ffffff;
        width:100%;
    }
    .nav-menu{
        padding-top: 25px;
    }

    .hamburger{
        display: none;
        cursor: pointer;
    }

    .bar{
        display: block;
        width: 25px;
        height: 3px;
        margin: 5px auto;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background-color: #FFFFFF;
    }

    @media(min-aspect-ratio: 16/9){
        .background-video{
            width: 100%;
            height: auto;
        }
    }
    @media(max-aspect-ratio: 16/9){
        .background-video{
            width: auto;
            height: 100%;
        }
    }
    @media(max-width:768px){
        .hamburger{
            display: block;
        }

        .hamburger.active .bar:nth-child(2){
            opacity: 0;
        }
        .hamburger.active .bar:nth-child(1){
            transform: translateY(8px) rotate(45deg);
        }
        .hamburger.active .bar:nth-child(3){
            transform: translateY(-8px) rotate(-45deg);
        }
        .nav-menu{
            padding-top: 150px;
            position: fixed;
            right: -100%;
            top: 0px;
            gap: 0;
            flex-direction: column;
            background: rgb(149,177,210);
            background: linear-gradient(200deg, rgba(65,94,125,1) 0%, rgba(48,61,88,1) 100%);
            width: 100%;
            text-align: left;
            transition: 0.3s;
            height: 100%;

        }
        .nav-item{
            display: list-item;
            margin: 20px 40px;
        }

        .nav-menu.active{
            right: 0;
            width: 60%;
            height: 100%;
        }
        .nav-item::after{
            content:'';
            display: none;
            width: 0;
            height: 2px;
            background: #ffffff;
            transition: width .3s;
        }
        .nav-item:hover::after{
            width:100%;
            transition: width .3s;
        }
    }
    .main-area{
        display: flex;
        width: 100%;
        height: 95%;
        padding-left: 20px;
        padding-right: 20px;
    }
    .content{
        width: 100%;
        height: 100%;
        margin-top: 10px;
        display: grid;
        grid-template-columns:1fr;
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.2));
        background-color: rgb(231, 235, 241);

    }
    @media(max-width:768px){
        .content{
            height: 98%;
        }
    }
    .grid-class{
        width: 100%;
        display: grid;
        grid-template-columns:1fr;
    }
    .side-menu{
        min-width: 250px;
        background-color: rgb(255, 255, 255);
    }
    .side-menu span{
        font-family: 'Roboto', sans-serif;
    }

    .side-menu-placement{
        width: 100%;
        height: 300px;
        margin-top: 5px;
    }

    .primary-footer {
        position: relative;
        margin-top: 25px;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 50px;
    }
    hr{
        width: 100%;
        background-color: black;
    }
    .footer-layout p{
        font-weight:600;
        font-size: 12px;
    }
    .footer-layout{
        margin: 0px auto;
        width: 95%;
    }
    .footer-items{
        display: flex;
        justify-content: space-between;
    }
    .footer-items a{
        text-decoration: none;

        font-size: 12px;
    }
    .left-section{
        text-align: left;

        font-size: 12px;
        margin-top: 5px;
    }
    .right-section{
        margin-top: 5px;
        text-align: right;
        min-width: 240px;
    }
    .hamburger-footer p{
        color: white;
        font-size: 12px;
    }
    .nav-itemp{
        display: none;
    }
    .nav-itemout{
        display: none;
    }
    .nav-itemx p{
        display: none;
    }
    @media(max-width:768px){
        .nav-itemp{
            text-align: left;
            display: block;
        }
        .nav-itemx p{
            display: block;
            margin-top: 20px;
            color: #FFFFFF;
            font-size: 10px;
        }
        .nav-itemp p{
            margin-top: 5px;
            font-size: 12px;
        }
        .nav-itemout{
            margin-bottom: 10px;
            display: block;
        }
        .nav-itemout a{
            font-size: 15px;
            padding: 5px 10px 5px 10px;
            border-color: #fff;
            border-style: solid;
            border-width: 2px;
            color: #ffffff;
            transition: 0.5s;
        }
        .nav-itemout a:hover{
            background-color: #FFFFFF;
            color: #000000;
        }
        .hamburger-footer{
            margin-top: 85%;
        }
        .primary-footer{
            display: none;
        }
        .side-menu{
            display: none;
        }
    }
    

    /* ------------------Side Bar Work------------------ */


    .side-menu-item1{
        background-color: #7f88ef;
        overflow: visible;
        width: 200px;
        height: 43px;
        position: absolute;
        left: 33px;
        top: 200px;
        border-radius: 6px;

    }

    /* ------------------ICONS------------------ */

    #Home_icon {
        text-align: center;
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        width: 38px;
        height: 35px;
        left: 39px;
        top: 152px;
        overflow: visible;
    }

    #SupplierInfo_icon {
        text-align: center;
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        width: 38px;
        height: 35px;
        left: 39px;
        top: 206px;
        overflow: visible;
    }
    #Message_icon {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        width: 38px;
        height: 35px;
        left: 39px;
        top: 260px;
        overflow: visible;
        text-align: center ;
    }
    #Setting_icon {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        width: 38px;
        height: 35px;
        left: 39px;
        top: 315px;
        overflow: visible;
        text-align: center ;
    }

    i.icon-home {
        margin-top: 4px;
        padding: 2px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border-radius: 6px;
        color: #ffffff;
        background-color: #e61b60;
        font-size: 24px;
    }
    i.icon-supplier{
        margin-top: 4px;
        padding: 2px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border-radius: 6px;
        color: #2a467b;
        background-color: #ffffff;
    }
    i.icon-setting{
        margin-top: 1px;
        padding: 2px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border-radius: 6px;
        color: #2a467b;
        background-color: #ffffff;
    }
    i.icon-message{
        margin-top: 1px;
        padding: 2px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border-radius: 6px;
        color: #2a467b;
        background-color: #ffffff;
    }


    /* ------------------ICONS-HOVER------------------ */

    a.sidebuttonhover{
        padding-left: 100px;
        padding-right: 100px;
        padding-bottom: 20px;
        border-radius: 6px;
        transition: 0.4s;
    }
    a.sidebuttonhover:hover{
        background: rgba(121, 121, 121, 0.13);
    }

    /* ------------------ICONS-TEXT------------------ */

    #Dashboard {
        left: 90px;
        top: 164px;
        position: absolute;
        overflow: visible;
        width: 64px;
        white-space: nowrap;
        line-height: 20px;
        margin-top: -3.5px;
        text-align: left;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        color: rgba(32, 33, 36, 1);
    }
    #Dashboard span{
        color: #000000;
    }
    #Message span{
        color: #000000;
    }
    #Supplier_Info span{
        color: white;
    }

    #Supplier_Info {
        left: 90px;
        top: 216px;
        position: absolute;
        overflow: visible;
        width: 73px;
        white-space: nowrap;
        line-height: 20px;
        margin-top: -3.5px;
        text-align: left;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        color: rgba(32, 33, 36, 1);
    }
    #Message {
        left: 90px;
        top: 270px;
        position: absolute;
        overflow: visible;
        width: 54px;
        white-space: nowrap;
        line-height: 20px;
        margin-top: -3.5px;
        text-align: left;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        color: rgba(32, 33, 36, 1);
    }
    #Setting {
        left: 90px;
        top: 326px;
        position: absolute;
        overflow: visible;
        width: 54px;
        white-space: nowrap;
        line-height: 20px;
        margin-top: -3.5px;
        text-align: left;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        color: rgba(32, 33, 36, 1);
    }

    /* ------------------ICONS-BUTTONS------------------ */


    .Dashboard_Button {
        position: absolute;
        overflow: visible;
        width: 200px;
        height: 43px;
        left: 33px;
        top: 148px;
    }

    .Supplier_Info_Button {
        position: absolute;
        overflow: visible;
        width: 200px;
        height: 43px;
        left: 33px;
        top: 200px;
    }
    .Setting_Button{
        position: absolute;
        overflow: visible;
        width: 194px;
        height: 43px;
        left: 33px;
        top: 306px;
        cursor: pointer;
    }
    .Message_Button{
        position: absolute;
        overflow: visible;
        width: 194px;
        height: 43px;
        left: 33px;
        top: 252px;
        cursor: pointer;
    }
    .ToTopButton{
        height: 80px;
        width: 80px;
        margin-left: 1310px;
        margin-top: 760px;
    }
    .upward{
        margin: 10px;
        padding-left: 5px;
        padding-right: 5px;
        padding-top: 4px;
        padding-bottom: 4px;
        background: #000000;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        cursor: pointer;
        z-index: 99;
        display: none;
    }
    .upward:hover{
        background: #4f4f4f;

    }
    i.icon-upward {
        margin:4px;
        border-radius: 50%;
        color: #ffffff;
    }
    i.icon-upward:hover {

        border-radius: 50%;
        color: #ffffff;
    }

    /*-------------------MAIL-INBOX-CONTAINER-------------------*/

    #List_Group_Desktop {
        width: 100%;
        height: 70vh;
        align-items: center;
        padding: 20px 30px 15px 30px;
    }
    .List_Group_Desktop_Inner{
        overflow-x: hidden;
        width: 100%;
        height: 100%;
        background-color: rgb(231, 235, 241);
    }
    /*-------------------TABLE-ALIGNMENT-------------------*/

    .List_Group_Desktop table {
        border-collapse: collapse;
        width: 100%;
        border-spacing: 20px;
    }


    .List_Group_Desktop td {
        font-family: 'Poppins', sans-serif;
        font-size: 14px;
        padding-top: 10px;
        padding-left: 5px;
        padding-bottom: 10px;
        text-align: left;
        padding-right: 5px;
    }

    .List_Group_Desktop th {
        font-family: 'Poppins', sans-serif;
        font-size: 15px;
        padding-left: 5px;
        text-align: left;
        padding-right: 5px;
        margin-left: 5px;
        margin-right: 5px;
        border-bottom: 2px solid #ddd;
        cursor: pointer;
    }
    .full_namex{
        width: 150px;
        word-break: break-word;
    }
    .phone_nox{
        width: 120px;
        word-break: break-word;
    }
    .emailx{
        width: 290px;
        word-break: break-word;
    }
    .subjectx{
        width: 360px;
        word-break: break-word;
    }
    .createdx{
        width: 260px;
        word-break: break-word;
    }
    .view{
        width: 40px;
    }
    .delete{
        width: 40px;
    }
    .List_Group_Desktop tbody tr:hover {
        background-color: #ffffff;
    }

    .List_Group_Desktop .sr-onlya i {
        background: #84d1ff;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .List_Group_Desktop .sr-onlya i:hover {
        background: #32b1fd;
    }
    .List_Group_Desktop .sr-onlyb i {
        background: #ff8383;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .List_Group_Desktop .sr-onlyb i:hover {
        background: #ff5444;
    }
    .List_Group_Desktop .actionbtn i {
        background: #84d1ff;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
    }
    .List_Group_Desktop .sr-onlyc i {
        background: #aaa3ff;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .List_Group_Desktop .sr-onlyc i:hover {
        background: #5849ff;
    }
    i.icon-edit{
        color: white;
    }
    i.icon-delete{
        color: white;
    }


    /*-------------------SEARCH-------------------*/

    #Search_bar_and_Add_button {
        margin-top: 100px;
        filter: drop-shadow(0px 4px 6px rgba(0, 0, 0, 0.2));
        position: relative;
        width: 100%;
        height: 57px;
        overflow: visible;
        background-color: rgba(112, 153, 255, 0.7);
    }
    .Search_Bar {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.2));
        position: relative;
        overflow: visible;
        width: 97%;
        height: 32px;
        border-radius: 50px;
        margin: 13px auto;
        background-color: rgba(206, 215, 255, 0.7);
    }
    .input.text{
        padding-top: 2px;
        width: 80%;
    }
    form{
        display: flex;
        justify-content: space-between;
        padding-left: 20px;
        padding-right: 20px;
    }
    .search_box {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        overflow: visible;
        width: 100%;
        height: 32px;
        z-index: -1;
        background: rgba(255,255,255,0);
    }
    .search_box input[type="text" i] {
        border: none;
        background: none;
        height: 20px;
        width:100%;
        padding-left: 2%;
        padding-right: 2%;
        outline: none;
    }
    .search-buttons{
        gap: 10px;
        padding-top: 2px;
        display: flex;
    }
    .buttona input[type=submit] {
        cursor: pointer;
        background: rgba(255, 255, 255, 0.43);
        width: 27px;
        height: 27px;
        border-radius: 50%;
        z-index: 1;
        border: none;
    }
    .buttona{
        z-index: 1;
        background-color: rgba(28, 195, 255, 0);
        margin-left: 0px;
        border: none;
    }
    .refresh{
        position: relative;
        overflow: visible;
        width: 27px;
        height: 27px;
        border-radius: 50%;
        background: rgba(255, 255, 255, 0.47);
    }
    .submit{
        position: absolute;
        top:2px;
    }
    i.icon-search{
        width: 27px;
        padding-left: 1.25px;
        padding-top: 0.75px;
        color: #000000;
    }
    i.icon-refresh {
        padding-left: 1.25px;
        padding-top: 0.75px;
        color: rgba(105, 105, 105, 0.9);
    }

    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #4a4a4a;
        opacity: 1; /* Firefox */
        font-family: 'Roboto', sans-serif;
    }

    .responsive-container{
        display: none;
    }
    
    .buttonexport{
        font-size: 15px;
        padding: 2px 10px 2px 10px;
        background-color: black;
        color: white;
        border-radius: 3px;
        text-decoration: none;
        transition: 0.5s;
    }
    .buttonexport:hover{
        background-color: #535353;
        color: white;
    }
    .export-csv{
        margin: auto;
        height: 35px;
    }

    @media(max-width:1000px){
        #List_Group_Desktop {
            padding-left: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 2.5px;
        }
        table{
            display: none;
        }
        .tablex-responsive{
            display: block;
        }
        .responsive-container{
            display: block;
        }
        .responsive-data{
            word-break: break-word;
        }
        .responsive-buttons{
            width: 100%;
            display: flex;
            gap: 10px;
        }
        .responsive-label{
            font-weight: 600;
        }
        .responsive-container-placeholder{
            padding: 20px;
            margin-bottom: 10px;
            border-radius: 5px;
            background-color: #dfdfdf;
            transition: 0.2s;
        }
        .responsive-container-placeholder:hover{
            background-color: #ffffff;
        }
        th{
            font-size: 17px;
            width: 100px;
            text-align: left;
        }
        .export-csv{
        margin: auto;
        height: 35px;
    }
        
    }
</style>
<body>

<header class="primary-header">
    <div class="hero">
        <nav>
            <?php
            foreach ($logox as $product){?>
            <?php echo
            $this->Html->image('/webroot/'.$product->image,
                array('class'=>'logo',
                    'url' => array('controller' => 'home', 'action' => 'dashboardPage'
                    )));
            ?>
            <?php
            }
            ?>
            <ul class="nav-menu">
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Dashboard</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['action' => 'dashboardPage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Mail Inbox</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['action' => 'contactmsgpage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Settings</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['action' => 'settingspage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><a href="#"></a></li>
                <?php
                foreach ($footerx as $footer){?>
                <div class="hamburger-footer">
                    <li class="nav-itemout"> <?php echo $this->Html->link('<span class="ITEMX1">Log Out</span><span class="sr-onlyxx">'
                            . __('') .
                            '</span>',['action' => 'logout'],
                            ['escape' => false, 'title' => __('')]) ?> </li>
                    <li class="nav-itemp"><p>ABN: <?=$footer->abn?></p</li>
                    <li class="nav-itemp"><p>ACR: <?=$footer->acr?> | AFCA: <?=$footer->afca?></p></li>
                    <li class="nav-itemp"><p>ACL: <?=$footer->acl?> | MFAA: <?=$footer->mfaa?></p></li>
                    <li class="nav-itemx"><p ><?=$footer->copyright?></p></li>

                </div>
                    <?php
                }
                ?>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>
</header>
        <div class="main-area">
            <div class="side-menu">
                <div class="side-menu-placement">

                    <div class="list-button">
                        <div class="side-menu-item1">

                        </div>
                        <div class="side-menu-item2">

                        </div>
                        <div class="side-menu-item3">

                        </div>
                        <div class="side-menu-item4">

                        </div>
                    </div>

                    <div id="Home_icon">
                        <i class="material-icons icon-home">home</i>
                    </div>
                    <div id="SupplierInfo_icon">
                        <i class="material-icons icon-message">mail</i>
                    </div>
                    <div id="Message_icon">
                        <i class="material-icons icon-setting">settings</i>
                    </div>
                    <div id="Setting_icon">

                    </div>
                    <div id="Dashboard">
                        <span>Dashboard</span>
                    </div>
                    <div id="Supplier_Info">
                        <span>Mail Inbox</span>
                    </div>
                    <div id="Message">
                        <span>Settings</span>
                    </div>
                    <div id="Setting">

                    </div>
                    <div class="Dashboard_Button">
                        <?= $this->Html->link(__(''), ['action' => 'dashboardPage'], ['class' => 'sidebuttonhover']) ?>
                    </div>
                    <div class="Supplier_Info_Button">

                    </div>
                    <div class="Message_Button">
                        <?= $this->Html->link(__(''), ['action' => 'settingspage'], ['class' => 'sidebuttonhover']) ?>
                    </div>

                </div>

            </div>
            <div class="grid-class">
                <div id="Search_bar_and_Add_button">


                    <div class="Search_Bar">
                        <div class = "search_box">
                            <?=$this->Form->create(null,["type"=>"get"]) ?>
                            <?=$this->Form->control("msgkey",["label"=>"","placeholder"=>"Type to search","value"=>$this->request->getQuery("msgkey")]) ?>

                            <div class="search-buttons">
                                <div class="searchxyz">

                                    <button class="buttona">
                                        <i class="material-icons icon-search">search<?= $this->Form->submit(__(''),
                                                ['escape' => false, 'title' => __('Search'),"class"=>"btn btn-xyz"]); ?>
                                        </i>
                                    </button>
                                    <?=$this->Form->end() ?>

                                </div>
                                <div class="refresh">
                                    <?php echo $this->Html->link('<span class="xyz-search"></span><span class="sr-onlyxyz">'
                                        . __('<i class="material-icons icon-refresh">restart_alt</i>') .
                                        '</span>',["onClick"=>"history.go(0)"],
                                        ['escape' => false, 'title' => __('Refresh'),"class"=>"btn btn-xyz"]) ?>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="content">


                    <div id="List_Group_Desktop" class="List_Group_Desktop">
                        <div class="List_Group_Desktop_Inner">
                            <div class="export-csv">
                                <?php echo $this->Html->link('CSV Export', ["action"=>"export"],
                                ['escape' => false, 'title' => __('Download CSV file'),"class"=>"buttonexport"]) ?>
                            </div>
                            <div class="responsive-container">
                                <?php
                                foreach ($messages as $msg_info){?>

                                    <div class="responsive-container-placeholder">

                                        <div class="responsive-message-area">
                                            <table class="tablex-responsive">
                                                <div class="responsive-buttons">
                                                    <div class="viewy">
                                                        <?php echo $this->Html->link('<span class="fa fa-search"></span><span class="sr-onlyc">'
                                                            . __('<i class="material-icons icon-edit">description</i>') .
                                                            '</span>',["action"=>"messageview",$msg_info->id],
                                                            ['escape' => false, 'title' => __('View Message'),"class"=>"btn btn-view2"]) ?>
                                                    </div>

                                                    <div class = "deletey">
                                                        <?php echo $this->Form->postLink('<span class="fa fa-search"></span><span class="sr-onlyb">'
                                                            . __('<i class="material-icons icon-delete">clear</i>') .
                                                            '</span>',["action"=>"deletemsg",$msg_info->id],
                                                            ['escape' => false, 'title' => __('Delete Message'),"class"=>"btn btn-danger","confirm"=>"Are you sure you want to delete this message?"]) ?>
                                                    </div>
                                                </div>
                                                <tr>
                                                    <th class="responsive-label"><?= __('Received') ?></th>
                                                    <td class="responsive-data">
                                                                <?=$msg_info->created->i18nFormat("MMMM dd, yyyy | KK:mm:ss a" ,"Australia/Melbourne") ?> |
                                                                <span class="msgstatusx"><?= h($msg_info->msgstatus) ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="responsive-label"><?= __('First Name') ?></th>
                                                    <td class="responsive-data"><?= h($msg_info->first_name) ?></td>
                                                </tr>
                                                <tr>
                                                    <th class="responsive-label"><?= __('Last Name') ?></th>
                                                    <td class="responsive-data"><?= h($msg_info->last_name) ?></td>
                                                </tr>
                                                <tr>
                                                    <th class="responsive-label"><?= __('Email') ?></th>
                                                    <td class="responsive-data"><?= h($msg_info->email) ?></td>
                                                </tr>
                                                <tr>
                                                    <th class="responsive-label"><?= __('Service') ?></th>
                                                    <td class="responsive-data"><?= h($msg_info->subject) ?></td>
                                                </tr>
                                            </table>
                                        </div>


                                    </div>



                                    <?php
                                }
                                ?>
                            </div>
                            <table id="myTable2">
                                <thead>
                                <tr>
                                    <th onclick="sortTable(0)">First Name</th>

                                    <th onclick="sortTable(1)">Last Name</th>

                                    <th onclick="sortTable(2)">Phone</th>

                                    <th onclick="sortTable(3)">Email</th>

                                    <th onclick="sortTable(4)">Service</th>

                                    <th onclick="sortTable(5)">Date</th>

                                    <th onclick="sortTable(6)">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($messages as $msg_info){?>

                                    <tr>

                                        <td class = "full_namex">
                                            <?=$msg_info->first_name ?>
                                        </td>
                                        <td class = "full_namex">
                                            <?=$msg_info->last_name ?>
                                        </td>
                                        <td class = "phone_nox">
                                            <?=$msg_info->phone_no ?>
                                        </td>

                                        <td class = "emailx">
                                            <?=$msg_info->email?>
                                        </td>
                                        <td class = "subjectx">
                                            <?=$msg_info->subject?>
                                        </td>

                                        <td class = "createdx">
                                            <?=$msg_info->created->i18nFormat("MMMM dd, yyyy | KK:mm:ss a" ,"Australia/Melbourne") ?>
                                        </td>

                                        <td class = "msgstatusx">
                                            <?=$msg_info->msgstatus?>
                                        </td>


                                        <td class="view">
                                            <?php echo $this->Html->link('<span class="fa fa-search"></span><span class="sr-onlyc">'
                                                . __('<i class="material-icons icon-edit">description</i>') .
                                                '</span>',["action"=>"messageview",$msg_info->id],
                                                ['escape' => false, 'title' => __('View Message'),"class"=>"btn btn-view2"]) ?>
                                        </td>

                                        <td class = "delete">
                                            <?php echo $this->Form->postLink('<span class="fa fa-search"></span><span class="sr-onlyb">'
                                                . __('<i class="material-icons icon-delete">clear</i>') .
                                                '</span>',["action"=>"deletemsg",$msg_info->id],
                                                ['escape' => false, 'title' => __('Delete Message'),"class"=>"btn btn-danger","confirm"=>"Are you sure you want to delete this message?"]) ?>
                                        </td>

                                    </tr>

                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

<footer>
    <div class="primary-footer" id="page-bottom">
        <div class="footer-layout">
            <div class="footer-margin">
                <hr>
                <?php
                foreach ($footerx as $footer){?>

                    <div class="footer-items">
                        <div class="left-section">
                            <p class="copyright2"><?=$footer->copyright?></p>
                        </div>
                        <div class="center-section">

                        </div>
                        <div class="right-section">
                            <p class="security-no">ABN: <?=$footer->abn?> | ACR: <?=$footer->acr?> | ACL: <?=$footer->acl?> | AFCA: <?=$footer->afca?> | MFAA: <?=$footer->mfaa?></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</footer>


</body>
<script type="text/javascript">
    const hamburger = document.querySelector(".hamburger");
    const navMenu = document.querySelector(".nav-menu");

    hamburger.addEventListener("click",()=>{
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
    })
</script>
<script>
    $("td.msgstatusx:contains('Not Addressed')").css("color", "#ec1d58");
    $("td.msgstatusx:contains('Working')").css("color", "#0d9d24");
    $("td.msgstatusx:contains('Solved')").css("color", "#8076ec");
</script>
<script>
    $(".msgstatusx:contains('Not Addressed')").css("color", "#ec1d58");
    $(".msgstatusx:contains('Working')").css("color", "#0d9d24");
    $(".msgstatusx:contains('Solved')").css("color", "#8076ec");
</script>

<script>
    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("myTable2");
        switching = true;
        // Set the sorting direction to ascending:
        dir = "asc";
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /* Check if the two rows should switch place,
                based on the direction, asc or desc: */
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                // Each time a switch is done, increase this count by 1:
                switchcount ++;
            } else {
                /* If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again. */
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }


</script>

<script>
    var mybutton = document.getElementById("myBtn");
    List_Group_Desktop.onscroll = function() {scrollFunction()};
    function scrollFunction() {
        if (List_Group_Desktop.scrollTop > 100 || List_Group_Desktop.scrollTop > 100) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }
    function scrollToTop() {
        List_Group_Desktop.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }

</script>
</html>

