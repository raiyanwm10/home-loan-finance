<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <title>Home Loan Finance | Mortgage Repayment Calculator</title>
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
</head>
<style>
    *{
        margin: 0%;
        padding: 0%;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
        scroll-behavior: smooth;
    }

    .hero{
        width: 100%;
        height: 100vh;
        background-image: linear-gradient(rgba(255, 255, 255, 0.0),rgba(255, 255, 255, 0.0));
        position: relative;
        align-items: center;
        min-height: 800px;

    }
    nav{
        width: 100%;
        position: fixed;
        background: rgb(149,177,210);
        background: linear-gradient(90deg, rgba(149,177,210,1) 0%, rgba(65,94,125,1) 100%);
        top: 0;
        left: 0;
        padding: 10px 8%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        transition: 0.6s;
        z-index: +1;
        font-family: 'Poppins', sans-serif;
    }

    nav .logo{
        position: relative;
        width: 130px;
        transition: 0.6s;
    }
    nav ul li {
        list-style: none;
        display: inline-block;
        margin-left: 40px;
        transition: 0.6s;
    }

    nav ul li a{
        text-decoration: none;
        color: #fff;
        font-size: 17px;
        transition: 0.6s;
    }
    .nav-item::after{
        content:'';
        display: block;
        width: 0;
        height: 2px;
        background: #ffffff;
        transition: width .3s;
    }
    .nav-item:hover::after{
        width:100%;
        transition: width .3s;
    }
    .nav-itema::after{
        content:'';
        display: block;
        height: 2px;
        background: #ffffff;
        width:100%;
    }

    .nav-menu{
        padding-top: 25px;
    }

    .hamburger{
        display: none;
        cursor: pointer;
    }

    .bar{
        display: block;
        width: 25px;
        height: 3px;
        margin: 5px auto;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background-color: #FFFFFF;
    }
    .familyimg{
        min-height: 800px;
        width: 100%;
        height: 100%;
        object-fit: cover;
        right: 0%;
        bottom: 0%;
        z-index: -1;
    }
    @media(max-width:850px){
        .hamburger{
            display: block;
        }

        .hamburger.active .bar:nth-child(2){
            opacity: 0;
        }
        .hamburger.active .bar:nth-child(1){
            transform: translateY(8px) rotate(45deg);
        }
        .hamburger.active .bar:nth-child(3){
            transform: translateY(-8px) rotate(-45deg);
        }
        .nav-menu{
            padding-top: 150px;
            position: fixed;
            right: -100%;
            top: 0px;
            gap: 0;
            flex-direction: column;
            background: rgb(149,177,210);
            background: linear-gradient(200deg, rgba(65,94,125,1) 0%, rgba(48,61,88,1) 100%);
            width: 100%;
            text-align: left;
            transition: 0.3s;
            height: 100%;

        }
        .nav-item{
            display: list-item;
            margin: 20px 40px;
        }
        .nav-menu.active{
            right: 0;
            width: 60%;
            height: 100%;
        }
        .nav-item::after{
            content:'';
            display: none;
            width: 0;
            height: 2px;
            background: #ffffff;
            transition: width .3s;
        }
        .nav-item:hover::after{
            width:100%;
            transition: width .3s;
        }
    }
    .primary-footer {
        background: rgb(132, 153, 178);
        width: 100%;
        height: 110px;
        bottom: 0px;
        clear: both;
        position: relative;
        margin-top: -0px;
    }
    hr{
        width: 100%;
        background-color: white;
    }
    .footer-layout p{
        font-size: 12px;
        color: #FFFFFF;
    }
    .footer-margin{
        width: 83.75%;
        position: absolute;
        margin: 40px auto;
    }
    .footer-layout{
        margin: 40px auto;
        width: 83.75%;
    }
    .footer-items{
        display: flex;
        justify-content: space-between;
    }
    .copyright2{
        display: none;
    }
    .footer-items a{
        text-decoration: none;
        color: #fff;
        font-size: 12px;
    }
    .left-section{
        text-align: left;
        color: #fff;
        font-size: 12px;
        margin-top: 5px;
    }
    .right-section{
        margin-top: 5px;
        text-align: right;
        min-width: 240px;
    }
    .hamburger-footer p{
        color: white;
        font-size: 12px;
    }
    .nav-itemp{
        display: none;
    }
    .nav-itemx p{
        display: none;
    }
    @media(max-width:850px){
        .nav-itemp{
            text-align: left;
            display: block;
        }
        .nav-itemx p{
            display: block;
            margin-top: 20px;
            color: #FFFFFF;
            font-size: 10px;
        }
        .nav-itemp p{
            margin-top: 5px;
            font-size: 12px;
        }
        .hamburger-footer{
            margin-top: 85%;
        }
        .primary-footer{
            height: 2px;
            background-color: #FFFFFF;
        }
        .footer-items{
            display: none;
        }
        hr{
            display: none;
        }
    }

    @media(max-width:1010px){
        .copyright2{
            display: block;
        }
        .emailandphone{
            display: none;
        }
        .address1{
            display: none;
        }
        .copyright1{
            display: none;
        }
    }
    .top-339-top{

    }
    .content{
        position: absolute;
        top: 40%;
        width: 100%;
        text-align: left;
        padding: 0px 4.1%;
    }
    .content-inside{
        background: rgb(149,177,210);
        background: linear-gradient(200deg, rgba(65,94,125,0) 0%, rgba(48,61,88,0.45) 100%);
        border-radius: 5px;
        padding: 25px 5% 50px;
    }
    .content h1{
        font-size: 54.5px;
        color: #fff;
        font-weight: 600;
        transition: 0.5s;
    }
    .content h2{
        font-size: 27.35px;
        color: #fff;
        font-weight: 600;
    }
    .button-area{
        margin-top: 30px;
    }
    .button-area2{
        margin:20px 0px;
    }
    .appointment-button a {
        font-weight: 600;
        border-radius: 5px;
        text-decoration: none;
        padding: 10px 25px 10px 25px;
        color: rgb(255, 255, 255);
        transition: 0.5s;
        border-style:solid;
        border-width: 3px;
        border-color: #FFFFFF;
    }
    .appointment-button a:hover {
        border-color: #ffffff;
        border-style:solid;
        border-width: 3px;
        background-color:rgb(255, 255, 255);
        color: rgb(0, 0, 0);
    }
    .appointment-button2 a {
        font-weight: 600;
        border-radius: 5px;
        text-decoration: none;
        padding: 9px 25px 9px 25px;
        color: white;
        transition: 0.5s;
        background-color: rgba(65,94,125,1);
        font-size:13px;
        border-color: #ffffff;
        border-style: none;
    }
    .appointment-button2 a:hover {
        color: white;
        background-color: rgb(75, 126, 176);
    }
    .calculate-button {
        font-size:13px;
        font-weight: 600;
        margin-top: 15px;
        margin-bottom: 15px;
        padding: 8px 50px 8px 50px;
        border-radius: 5px;
        cursor: pointer;
        transition: 0.5s;
        background-color: rgba(65,94,125,1);
        color: white;
        border-color: #ffffff;
        border-style: none;
    }
    .calculate-button:hover {
        background-color: rgb(75, 126, 176);
        color: white;
    }
    @media(max-width:625px){
        .breaktext{
            display: none;
        }
    }
    .services{
        width: 100%;
        height: 100%;
        position: relative;
        margin-top: -100px;
    }
    .services-container{
        width: 91.75%;
        margin: 15px auto;
        background: rgb(149,177,210);
        background: linear-gradient(0deg, rgb(229, 234, 241) 0%, rgb(255, 255, 255) 100%);
        border-radius: 10px;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .services-text{
        padding: 0px 0px 20px;
        text-align: center;
        color: rgba(65,94,125,1);
        font-size: 20px;
    }
    .services-main-container{
        padding: 1.5% 5% 2.5%;
    }
    .services-layout{
        display: grid;
        grid-template-columns: 1fr 3fr;
        gap: 50px;
    }
   .calculator-results1{
        margin-top: 10px;
        gap: 20px;
        border-radius: 4px;
        background-color: #e8e8e8;
        padding: 20px;
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
    }
    .results123{
        padding: 10px 25px;
        background-color: white;
        border-radius: 4px;
    }
    .results123 h3{
        background: rgb(210, 219, 239);
        color: rgb(26, 38, 62);
        text-align: center;
        border-radius: 2px;
    }
    .results123 .is-4{
        color: rgb(26, 38, 62);
        text-align: right;
        font-weight: 600;
        padding: 13px 12px 12px 12px;
        font-size: 17px;
    }
    .results123 .is-4 span{
        font-size: 15px;
        color: rgb(102, 120, 158);
    }
    .text2 h2{
        color: rgba(48,61,88,1);
    }
    .text2 h3{
        color: rgba(48,61,88,1);
    }
    .services-text2{
        font-size: 15px;
        padding: 2.5px;
        text-align: center;
        background-color: rgb(210, 219, 239);
        border-radius: 4px;
    }
   @media(max-width:1100px){
        .services-layout{
            display: block;
        }

    }
    @media(max-width:850px){
        .content h1{
            font-size: 35px;
        }
        .content h2{
            font-size: 17.35px;
        }
        .services-text{
            font-size: 18px;
        }
        .appointment-button a {
            padding: 7.5px 17px 7.5px 17px;
        }
        .services-text2{
        font-size: 12px;
        }
        .calculator-results1{
            display: block;
        }
        .results123{
            padding: 10px 0px;
        }
        .results123:not(:last-child){
            margin: 0px 0px 20px 0px;
        }
        .results123 h3{
            background: rgb(255, 255, 255);
            color: rgb(73, 91, 157);

        }
        .brx{
            display: none;
        }
        .results123 .is-4{
            text-align: left;
        }
        .results123 .is-4 span{
            text-align: right;
        }
        .content h1:hover{
        -webkit-text-stroke: 2px #fff;
        color: transparent;
        }
    }
    .button-layouts{
        display: flex;
        justify-content: space-between;
    }
    .icon-down{
        border-style: solid;
        border-color: rgba(48,61,88,1);
        border-width: 3px;
        border-radius: 50%;
        padding: 5px;
        color: white;
        font-size: 30px;
        cursor: pointer;
        transition: 0.3s;
        background-color: rgba(48,61,88,1);
    }
    .icon-down:hover{
        border-color: rgb(0, 0, 0);
        background-color: black;
        color: #ffffff;
        cursor: pointer;
    }
    .downward{
        margin-top: 20px;
        background-color: rgba(0, 0, 0, 0);
        border-radius: 50%;
        border-style: none;
    }
    input[type=number],select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=currency],select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    .level-item {
        font-weight: 600;
        padding: 12px 12px 12px 0;
        display: inline-block;
    }
    .errormsgx{
        font-size: 12px;
        color: red;
    }
    .statusupdatediv{
        padding-top: 10px;
    }
    span#statusupdate.title.is-1{
        font-weight: 600;
        font-size: 15px;
        color: rgb(102, 120, 158);
    }
    .statusupdatearea{
        text-align: center;
    }
    #appointmentxyz{
        text-align: center;
        display: none;
    }
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    .unit {
        padding: 0 5px;
        border-radius: 2px;
        text-align: center;
        position: absolute;
        display: block;
        top: 10px;
        right: 5px;
        background-color: #395464;
        color: #ffffff;
        width: 75px;
    }
    .input-container {
        position: relative;
        width: 100%;
    }
    .input-container input {
        width: 100%;
    }
</style>
<body>

<header class="primary-header">
    <div class="hero" id="hero">
        <img src="<?php echo APPLICATION_URL ?>/webroot/images/family5.jpg" class="familyimg">
        <div class="content">
            <div class="content-inside">
                <div class="headline-text">
                    <h1>MORTGAGE REPAYMENT CALCULATOR</h1>
                    <h2>Calculate your estimated loan repayments, connect <br class="breaktext"> with us today for refinancing</h2>
                </div>
                <div class="button-layouts">
                    <div class="button-area">
                        <div class="appointment-button"><a <?php echo $this->Html->link('<span class="ITEMX1">BOOK AN APPOINTMENT</span><span class="sr-onlyxx">'
                                . __('') .
                                '</span>',["action"=>"contact"],
                                ['escape' => false, 'title' => __('')]) ?> </a></div>
                    </div>
                    <div class = "TodownButton">
                        <button class="downward" title="See Calculator" id="scroll-to-bottom">
                            <i class ="material-icons icon-down">keyboard_double_arrow_down</i>
                        </button>
                    </div>
                </div>
            </div>


        </div>
        <nav>
            <?php
            foreach ($logox as $product){?>
                <?php echo
                $this->Html->image('/webroot/'.$product->image,
                    array('class'=>'logo',
                        'url' => array('controller' => 'home', 'action' => 'landingpage'
                        )));
                ?>
                <?php
            }
            ?>
            <ul class="nav-menu">
                <li class="nav-item"> <?php echo $this->Html->link('<span class="ITEMX1">HOME</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"landingpage"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <li class="nav-item"> <?php echo $this->Html->link('<span class="ITEMX1">SERVICES</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"services"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <li class="nav-itema"> <?php echo $this->Html->link('<span class="ITEMX1">CALCULATOR</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"calculatorone"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <li class="nav-item"> <?php echo $this->Html->link('<span class="ITEMX1">CONTACT</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"contact"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <?php
                foreach ($footerx as $footer){?>
                    <div class="hamburger-footer">

                        <li class="nav-itemp"><p>ABN: <?=$footer->abn?></p</li>
                        <li class="nav-itemp"><p>ACR: <?=$footer->acr?> | AFCA: <?=$footer->afca?></p></li>
                        <li class="nav-itemp"><p>ACL: <?=$footer->acl?> | MFAA: <?=$footer->mfaa?></p></li>
                        <li class="nav-itemx"><p ><?=$footer->copyright?></p></li>
                    </div>
                    <?php
                }
                ?>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>


    </div>
</header>

<section class="services">

    <div class="services-container">

        <div class="content-services">

            <div class="services-main-container">
                <div class="services-text">
                    <h1>Calculate your loan repayment</h1>
                </div>
                <div class="services-layout">
                    <div class="calculator-inputs">
                        <div class="services-text2">
                            <h1>Please input your loan details</h1>
                        </div>
                        <div id="loan-form">
                            <div class="calculator-input-layout">
                                <div class="level">

                                    <div class="level-item">
                                        Loan Amount
                                    </div>
                                    <div class="input-container">
                                        <input class="input" id="amount" type="number" min="20000" max="99999999" maxlength="9"
                                               oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" placeholder="$" />
                                        <span class="unit">Dollars</span>
                                    </div>
                                    <div>
                                        <span id="errormsg1" class="errormsgx"></span>
                                    </div>
                                    <span class="icon is-small is-left">
                          <i class="fa fa-dollar-sign"></i>
                          </span>

                                </div>

                                <div class="level">

                                    <div class="level-item">
                                        Interest Rate Per Annum
                                    </div>
                                    <div class="input-container">
                                    <input class="input" id="interest" type="number" min="0.01" max="24.99" step="0.01" maxlength="5"
                                           oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" placeholder="%" />
                                    <span class="unit">% Pa</span>
                                    </div>
                                    <div>
                                        <span id="errormsg2" class="errormsgx"></span>
                                    </div>
                                    <span class="icon is-small is-right">
                        <i class="fa fa-percentage"></i>
                        </span>

                                </div>

                                <div class="level">
                                    <div class="level-item">
                                        Loan Term
                                    </div>
                                    <div class="input-container">
                                    <input class="input" id="years" type="number" min="1" max="39" maxlength="2"
                                           oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" placeholder="Years" >
                                    <span class="unit">YEARS</span>
                                    </div>
                                    <div>
                                        <span id="errormsg3" class="errormsgx"></span>
                                    </div>
                                    <span class="icon is-small is-left">
                        <i class="fa fa-calendar"></i>
                        </span>
                                </div>

                                <div class="control">
                                    <button class="calculate-button" onclick="myFunction()">
                                        Calculate
                                    </button>
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="calculator-results">
                        <div class="services-text2">
                            <h1>Your Repayments</h1>
                        </div>
                        <div class="calculator-results1">
                            <div class="results123">
                                <h3>Monthly</h3>
                                <p class="subtitle is-4">Monthly Payments<br><span id="monthlyPayment" class="title is-1">$ No input Given</span></p>
                                <p class="subtitle is-4">Total Interest<br><span id="totalInterest" class="title is-1">$ No input Given</span></p>
                                <p class="subtitle is-4">Total Repayment<br><span id="totalInterestmonthlywithprincipal" class="title is-1">$ No input Given</span></p>
                            </div>
                            <div class="results123">
                                <h3>Fortnightly</h3>
                                <p class="subtitle is-4">Fortnightly Payments<br><span id="fortnightlyPayment" class="title is-1">$ No input Given</span></p>
                                <p class="subtitle is-4">Total Interest<br><span id="totalInterestfortnightly" class="title is-1">$ No input Given</span></p>
                                <p class="subtitle is-4">Total Repayment<br><span id="totalInterestfortnightlywithprincipal" class="title is-1">$ No input Given</span></p>
                            </div>
                            <div class="results123">
                                <h3>Weekly</h3>
                                <p class="subtitle is-4">Weekly Payments<br><span id="weeklyPayment" class="title is-1">$ No input Given</span></p>
                                <p class="subtitle is-4">Total Interest<br><span id="totalInterestweekly" class="title is-1">$ No input Given</span></p>
                                <p class="subtitle is-4">Total Repayment<br><span id="totalInterestweeklywithprincipal"" class="title is-1">$ No input Given</span></p>
                            </div>
                        </div>
                        <div>
                            <div class="statusupdatediv">
                                <p class="statusupdatearea"><span id="statusupdate" class="title is-1"></span></p>
                            </div>
                            <div class="button-area2" id="appointmentxyz" >
                                <div class="appointment-button2"><a <?php echo $this->Html->link('<span class="ITEMX1">BOOK AN APPOINTMENT</span><span class="sr-onlyxx">'
                                        . __('') .
                                        '</span>',["action"=>"contact"],
                                        ['escape' => false, 'title' => __('')]) ?> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



        </div>

    </div>

</section>
<footer>
    <div class="primary-footer" id="page-bottom">
        <div class="footer-layout">
            <div class="footer-margin">
                <hr>
                <?php
                foreach ($footerx as $footer){?>

                    <div class="footer-items">
                        <div class="left-section">

                            <span class="emailandphone">Email: <?=$footer->email?> | Phone: <?=$footer->phone?></span>
                            <p class="address1">Address: <?=$footer->address?></p>
                            <p class="copyright2"><?=$footer->copyright?></p>
                        </div>
                        <div class="center-section">

                        </div>
                        <div class="right-section">
                            <p class="security-no">ABN: <?=$footer->abn?> | ACR: <?=$footer->acr?> | ACL: <?=$footer->acl?> | AFCA: <?=$footer->afca?> | MFAA: <?=$footer->mfaa?></p>
                            <p class="copyright1"><?=$footer->copyright?></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</footer>




</body>




<script type="text/javascript">
    const hamburger = document.querySelector(".hamburger");
    const navMenu = document.querySelector(".nav-menu");

    hamburger.addEventListener("click",()=>{
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
    })
</script>
<script>
    let scrollToBottom = document.querySelector("#scroll-to-bottom")
    let pageBottom = document.querySelector("#page-bottom")

    scrollToBottom.addEventListener("click", function() {
        pageBottom.scrollIntoView()
    })
</script>
<script src="<?php echo APPLICATION_URL ?>/webroot/js/calculatorapp1.js"></script>
<script type="text/javascript">
    window.addEventListener("scroll",function(){
        var nav = document.querySelector("nav");
        nav.classList.toggle("sticky",window.scrollY>0)
    })
</script>
<script>
    function myFunction() {
        var loan = document.getElementById("amount").value;
        var interest = document.getElementById("interest").value;
        var term = document.getElementById("years").value;

        if(loan >= 20001 && loan <= 999999999 && interest >= 0.01 && interest <= 24.99 && term >= 1 && term <=39){

            document.getElementById("errormsg1").innerHTML= "";
            document.getElementById("errormsg3").innerHTML= "";
            document.getElementById("errormsg2").innerHTML= "";
            document.getElementById("statusupdate").innerHTML =
                "For more details, please send us an enquiry";
            document.getElementById('appointmentxyz').style.display = "block";
            computeResults();
        }


        else{
            if (loan<20001){
                document.getElementById("errormsg1").innerHTML= "Loan amount must be greater than $20000";
            }
            if (loan>=20001){
                document.getElementById("errormsg1").innerHTML= "";
            }
            if (term > 39 ){
                document.getElementById("errormsg3").innerHTML= "Loan term must be greater than 0 and lower than 40";
            }
            if (term < 1  ){
                document.getElementById("errormsg3").innerHTML= "Loan term must be greater than 0 and lower than 40";
            }
            if (term < 40 && term > 0){
                document.getElementById("errormsg3").innerHTML= "";
            }
            if (interest > 24.99){
                document.getElementById("errormsg2").innerHTML= "Interest rate must be greater than 0 and lower than 25";
            }
            if (interest < 0.01){
                document.getElementById("errormsg2").innerHTML= "Interest rate must be greater than 0 and lower than 25";
            }
            if (interest >= 0.01 && interest <= 24.99){
                document.getElementById("errormsg2").innerHTML= "";
            }


            document.getElementById("statusupdate").innerHTML =
                "";

        }

    }
</script>
</html>
