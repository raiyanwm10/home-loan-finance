<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <title>Home Loan Finance | Contact Us</title>
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
</head>
<style>
    *{
        margin: 0%;
        padding: 0%;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
        scroll-behavior: smooth;
    }

    .hero{
        width: 100%;
        height: 100vh;
        background-image: linear-gradient(rgba(255, 255, 255, 0.0),rgba(255, 255, 255, 0.0));
        position: relative;
        align-items: center;
        min-height: 800px;

    }
    nav{
        width: 100%;
        position: fixed;
        background: rgb(149,177,210);
        background: linear-gradient(90deg, rgba(149,177,210,1) 0%, rgba(65,94,125,1) 100%);
        top: 0;
        left: 0;
        padding: 10px 8%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        transition: 0.6s;
        z-index: +1;
        font-family: 'Poppins', sans-serif;
    }

    nav .logo{
        position: relative;
        width: 130px;
        transition: 0.6s;
    }
    nav ul li {
        list-style: none;
        display: inline-block;
        margin-left: 40px;
        transition: 0.6s;
    }

    nav ul li a{
        text-decoration: none;
        color: #fff;
        font-size: 17px;
        transition: 0.6s;
    }
    .nav-item::after{
        content:'';
        display: block;
        width: 0;
        height: 2px;
        background: #ffffff;
        transition: width .3s;
    }
    .nav-item:hover::after{
        width:100%;
        transition: width .3s;
    }
    .nav-itema::after{
        content:'';
        display: block;
        height: 2px;
        background: #ffffff;
        width:100%;
    }

    .nav-menu{
        padding-top: 25px;
    }

    .hamburger{
        display: none;
        cursor: pointer;
    }

    .bar{
        display: block;
        width: 25px;
        height: 3px;
        margin: 5px auto;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background-color: #FFFFFF;
    }
    .familyimg{
        min-height: 800px;
        width: 100%;
        height: 100%;
        object-fit: cover;
        right: 0%;
        bottom: 0%;
        z-index: -1;
    }
    @media(max-width:850px){
        .hamburger{
            display: block;
        }

        .hamburger.active .bar:nth-child(2){
            opacity: 0;
        }
        .hamburger.active .bar:nth-child(1){
            transform: translateY(8px) rotate(45deg);
        }
        .hamburger.active .bar:nth-child(3){
            transform: translateY(-8px) rotate(-45deg);
        }
        .nav-menu{
            padding-top: 150px;
            position: fixed;
            right: -100%;
            top: 0px;
            gap: 0;
            flex-direction: column;
            background: rgb(149,177,210);
            background: linear-gradient(200deg, rgba(65,94,125,1) 0%, rgba(48,61,88,1) 100%);
            width: 100%;
            text-align: left;
            transition: 0.3s;
            height: 100%;

        }
        .nav-item{
            display: list-item;
            margin: 20px 40px;
        }
        .nav-menu.active{
            right: 0;
            width: 60%;
            height: 100%;
        }
        .nav-item::after{
            content:'';
            display: none;
            width: 0;
            height: 2px;
            background: #ffffff;
            transition: width .3s;
        }
        .nav-item:hover::after{
            width:100%;
            transition: width .3s;
        }
    }
    .primary-footer {
        background: rgb(132, 153, 178);
        width: 100%;
        height: 110px;
        bottom: 0px;
        clear: both;
        position: relative;
        margin-top: -0px;
    }
    hr{
        width: 100%;
        background-color: white;
    }
    .footer-layout p{
        font-size: 12px;
        color: #FFFFFF;
    }
    .footer-margin{
        width: 83.75%;
        position: absolute;
        margin: 40px auto;
    }
    .footer-layout{
        margin: 40px auto;
        width: 83.75%;
    }
    .footer-items{
        display: flex;
        justify-content: space-between;
    }
    .copyright2{
        display: none;
    }
    .footer-items a{
        text-decoration: none;
        color: #fff;
        font-size: 12px;
    }
    .left-section{
        text-align: left;
        color: #fff;
        font-size: 12px;
        margin-top: 5px;
    }
    .right-section{
        margin-top: 5px;
        text-align: right;
        min-width: 240px;
    }
    .hamburger-footer p{
        color: white;
        font-size: 12px;
    }
    .nav-itemp{
        display: none;
    }
    .nav-itemx p{
        display: none;
    }
    @media(max-width:850px){
        .nav-itemp{
            text-align: left;
            display: block;
        }
        .nav-itemx p{
            display: block;
            margin-top: 20px;
            color: #FFFFFF;
            font-size: 10px;
        }
        .nav-itemp p{
            margin-top: 5px;
            font-size: 12px;
        }
        .hamburger-footer{
            margin-top: 85%;
        }
        .primary-footer{
            height: 2px;
            background-color: #FFFFFF;
        }
        .footer-items{
            display: none;
        }
        hr{
            display: none;
        }
    }

    @media(max-width:1010px){
        .copyright2{
            display: block;
        }
        .emailandphone{
            display: none;
        }
        .address1{
            display: none;
        }
        .copyright1{
            display: none;
        }
    }
    .top-339-top{

    }
    .content{
        position: absolute;
        top: 40%;
        width: 100%;
        text-align: left;
        padding: 0px 4.1%;
    }
    .content-inside{
        background: rgb(149,177,210);
        background: linear-gradient(200deg, rgba(65,94,125,0) 0%, rgba(48,61,88,0.45) 100%);
        border-radius: 5px;
        padding: 25px 5% 20px;
    }
    .content h1{
        font-size: 54.5px;
        color: #fff;
        font-weight: 600;
        transition: 0.5s;
    }
    .content h2{
        font-size: 27.35px;
        color: #fff;
        font-weight: 600;
    }
    .button-area{
        margin-top: 30px;
    }
    .appointment-button a {
        display:none;
        font-weight: 600;
        border-radius: 5px;
        text-decoration: none;
        padding: 10px 25px 10px 25px;
        color: rgb(255, 255, 255);
        transition: 0.5s;
        border-style:solid;
        border-width: 3px;
        border-color: #FFFFFF;
        cursor: pointer;
    }
    .appointment-button a:hover {
        border-color: #ffffff;
        border-style:solid;
        border-width: 3px;
        background-color:rgb(255, 255, 255);
        color: rgb(0, 0, 0);
    }
    @media(max-width:625px){
        .breaktext{
            display: none;
        }
    }
    .services{
        width: 100%;
        height: 100%;
        position: relative;
        margin-top: -100px;
    }
    .services-container{
        width: 91.75%;
        margin: 15px auto;
        background: rgb(149,177,210);
        background: linear-gradient(0deg, rgb(229, 234, 241) 0%, rgb(255, 255, 255) 100%);
        border-radius: 10px;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .services-text{
        padding: 0px 0px 20px;
        text-align: center;
        color: rgba(65,94,125,1);
        font-size: 20px;
    }
    .services-main-container{
        padding: 1.5% 5% 2.5%;
    }
    .services-layout{
        display: grid;
        grid-template-columns: 1fr 1fr;
        gap: 50px;
    }
    .text2 h2{
        color: rgba(48,61,88,1);
    }
    .text2 h3{
        color: rgba(48,61,88,1);
    }
    table th, td {
        color: white;
        font-weight: 600;
        font-size: 20px;
    }
    td{
        word-break: break-word;
        width: 420px;
        padding-left: 2%;
    }
    @media(max-width:1100px){
        .services-layout{
            display: block;
        }
        .service-container1:hover{
            background-color: #ffffff;
        }
    }
    @media(max-width:850px){
        .content h1{
            font-size: 35px;
        }
        .content h2{
            font-size: 17.35px;
        }
        .services-text{
            font-size: 18px;
        }
        .appointment-button a {
            padding: 7.5px 17px 7.5px 17px;
        }
        table th, td {
            color: white;
            font-weight: 600;
            font-size: 14px;
        }
        .content h1:hover{
        -webkit-text-stroke: 2px #fff;
        color: transparent;
        }
    }
    .button-layouts{
        display: flex;
        justify-content: space-between;
    }
    .content-layouttest{
        display: flex;
        justify-content: space-between;
    }
    .icon-down{
        border-style: solid;
        border-color: rgba(48,61,88,1);
        border-width: 3px;
        border-radius: 50%;
        padding: 5px;
        color: white;
        font-size: 30px;
        cursor: pointer;
        transition: 0.3s;
        background-color: rgba(48,61,88,1);
    }
    .icon-down:hover{
        border-color: rgb(0, 0, 0);
        background-color: black;
        color: #ffffff;
        cursor: pointer;
    }
    .downward{
        margin-top: 104px;
        background-color: rgba(0, 0, 0, 0);
        border-radius: 50%;
        border-style: none;
    }
    .contact-formx button{
        margin-top: 17px;
        padding: 8px 35px 8px 35px;
        background: linear-gradient(90deg, rgb(157, 174, 204) 0%, rgb(129, 161, 220) 100%);
        transition: 0.5s;
        border-color: #ffffff;
        border-style: none;
        cursor: pointer;
        border-radius: 5px;
        font-weight: 600;
        letter-spacing: 1px;
    }
    .contact-formx button:hover{
        color: white;
        background: linear-gradient(90deg, rgb(126, 143, 174) 0%, rgb(103, 141, 209) 100%);
    }
    input[type=email], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=text], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=number], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=float], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=tel], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=select], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }

    label {
        font-weight: 600;
        padding: 12px 12px 12px 0;
        display: inline-block;
    }
    .items-section{
        display: grid;
        grid-template-columns: 1fr 1fr;
        gap: 20px
    }
    @media(max-width:1340px){
        .items-section{
            display: block;
        }
    }
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

</style>
<body>

<header class="primary-header">
    <div class="hero" id="hero">
        <img src="<?php echo APPLICATION_URL ?>/webroot/images/family6.jpg" class="familyimg">
        <div class="content">
            <div class="content-inside">
                <div class="headline-text">
                    <h1>CONTACT US</h1>
                    <div class="content-layouttest">
                        <div>
                            <table>
                            <?php
                            foreach ($inventorys as $contact_info){?>

                                <tr>
                                    <th><?= __('Phone:') ?></th>
                                    <td id="phonex"><?=$contact_info->phone ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Enquire:') ?></th>
                                    <td class="break-word"><?=$contact_info->email?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Complain:') ?></th>
                                    <td class="break-word"><?=$contact_info->emailc?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Address:') ?></th>
                                    <td class="break-word"><?=$contact_info->address?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </table>
                        </div>
                        <div class = "TodownButton">
                            <button class="downward" title="See Mail Form" id="scroll-to-bottom">
                                <i class ="material-icons icon-down">keyboard_double_arrow_down</i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="button-layouts">
                    <div class="button-area">
                        <div class="appointment-button"><a id="scroll-to-bottom2">BOOK AN APPOINTMENT</a></div>
                    </div>
                    
                </div>
            </div>


        </div>
        <nav>
            <?php
            foreach ($logox as $product){?>
                <?php echo
                $this->Html->image('/webroot/'.$product->image,
                    array('class'=>'logo',
                        'url' => array('controller' => 'home', 'action' => 'landingpage'
                        )));
                ?>
                <?php
            }
            ?>
            <ul class="nav-menu">
                <li class="nav-item"> <?php echo $this->Html->link('<span class="ITEMX1">HOME</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"landingpage"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <li class="nav-item"> <?php echo $this->Html->link('<span class="ITEMX1">SERVICES</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"services"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <li class="nav-item"> <?php echo $this->Html->link('<span class="ITEMX1">CALCULATOR</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"calculatorone"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <li class="nav-itema"> <?php echo $this->Html->link('<span class="ITEMX1">CONTACT</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"contact"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <?php
                foreach ($footerx as $footer){?>
                    <div class="hamburger-footer">

                        <li class="nav-itemp"><p>ABN: <?=$footer->abn?></p</li>
                        <li class="nav-itemp"><p>ACR: <?=$footer->acr?> | AFCA: <?=$footer->afca?></p></li>
                        <li class="nav-itemp"><p>ACL: <?=$footer->acl?> | MFAA: <?=$footer->mfaa?></p></li>
                        <li class="nav-itemx"><p ><?=$footer->copyright?></p></li>
                    </div>
                    <?php
                }
                ?>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>


    </div>
</header>

<section class="services">

    <div class="services-container">

        <div class="content-services">

            <div class="services-main-container">
                <div class="services-text">
                    <h1>Send us an enquiry</h1>
                </div>
                <div class="services-layout">
                    <div class="contact-formx1">
                        <?= $this->Form->create($enquiry) ?>

                        <div class="items-section">
                            <?php
                            echo $this->Form->control('first_name', ['label' => 'First Name*']);
                            echo $this->Form->control('last_name', ['label' => 'Last Name*']);
                            ?>
                        </div>
                        <div class="items-section">
                            <?php
                            echo $this->Form->control('phone_no',["maxlength"=>"12",'required' => true,
                                'label'    => 'Mobile No*',
                                'type'=>"number",
                                'oninput'=>"javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"]);
                            echo $this->Form->control('email', ['label' => 'Email*']);
                            ?>
                        </div>
                        <?php
                        echo $this->Form->label('subject', __('Service*'));
                        echo $this->Form->select('subject',[
                            ""=>"Please select your required service",
                            "Refinance" => "Refinance",
                            "Purchase" => "Purchase",
                            "Residential Investment Advice" => "Residential Investment Advice",
                            "Debt Consolidation" => "Debt Consolidation",
                            "Home Loan Health Check" => "Home Loan Health Check",
                            "Complaint" => "Complaint",
                            "Other" => "Other"
                        ]);
                        ?>
                    </div>
                    <div class="contact-formx">
                        <?php
                        echo $this->Form->control('body', ['label' => 'Message*', 'rows' => 7]);
                        ?>

                        <?= $this->Form->button(__('Send')) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>

            </div>



        </div>

    </div>

</section>
<footer>
    <div class="primary-footer" id="page-bottom">
        <div class="footer-layout">
            <div class="footer-margin">
                <hr>
                <?php
                foreach ($footerx as $footer){?>

                    <div class="footer-items">
                        <div class="left-section">

                            <span class="emailandphone">Email: <?=$footer->email?> | Phone: <?=$footer->phone?></span>
                            <p class="address1">Address: <?=$footer->address?></p>
                            <p class="copyright2"><?=$footer->copyright?></p>
                        </div>
                        <div class="center-section">

                        </div>
                        <div class="right-section">
                            <p class="security-no">ABN: <?=$footer->abn?> | ACR: <?=$footer->acr?> | ACL: <?=$footer->acl?> | AFCA: <?=$footer->afca?> | MFAA: <?=$footer->mfaa?></p>
                            <p class="copyright1"><?=$footer->copyright?></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</footer>




</body>

<script type="text/javascript">
    const hamburger = document.querySelector(".hamburger");
    const navMenu = document.querySelector(".nav-menu");

    hamburger.addEventListener("click",()=>{
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
    })
</script>
<script>
    let scrollToBottom = document.querySelector("#scroll-to-bottom")
    let pageBottom = document.querySelector("#page-bottom")

    scrollToBottom.addEventListener("click", function() {
        pageBottom.scrollIntoView()
    })
</script>
<script>
    let scrollToBottom2 = document.querySelector("#scroll-to-bottom2")
    let pageBottom2 = document.querySelector("#page-bottom")

    scrollToBottom2.addEventListener("click", function() {
        pageBottom2.scrollIntoView()
    })
</script>
</html>

