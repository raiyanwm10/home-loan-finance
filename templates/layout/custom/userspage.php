<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <title>Home Loan Finance | Users List</title>
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
</head>
<style>
    *{
        margin: 0%;
        padding: 0%;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
    }
    ::-webkit-scrollbar{
        width: 10px;
    }
    ::-webkit-scrollbar-thumb{
        background: #86a8fd;
        border-radius: 10px;
    }

    ::-webkit-scrollbar-thumb:hover  {
        background: #86a8fd;

    }

    .hero{
        width: 100%;
        height: 5vh;
        background-image: linear-gradient(rgba(255, 255, 255, 0.0),rgba(255, 255, 255, 0.0));
        position: relative;
        display: flex;
        align-items: center;
        background-color: rgb(255, 255, 255);
    }
    nav{
        width: 100%;
        background: rgb(149,177,210);
        background: linear-gradient(90deg, rgba(149,177,210,1) 0%, rgba(65,94,125,1) 100%);
        position: fixed;
        top: 0;
        left: 0;
        padding: 10px 4%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        transition: 0.6s;
        font-family: 'Poppins', sans-serif;
        z-index: +1;
    }

    nav .logo{
        position: relative;
        width: 130px;
        transition: 0.6s;
    }

    nav ul li {
        list-style: none;
        display: inline-block;
        margin-left: 40px;
        transition: 0.6s;
    }

    nav ul li a{
        text-decoration: none;
        color: #fff;
        font-size: 17px;
        transition: 0.6s;
    }
    .nav-item{
        display: none;
    }
    .nav-item::after{
        content:'';
        display: block;
        width: 0;
        height: 2px;
        background: #ffffff;
        transition: width .3s;
    }
    .nav-item:hover::after{
        width:100%;
        transition: width .3s;
    }
    .nav-itema::after{
        content:'';
        display: block;
        height: 2px;
        background: #ffffff;
        width:100%;
    }
    .nav-menu{
        padding-top: 25px;
    }

    .hamburger{
        display: none;
        cursor: pointer;
    }

    .bar{
        display: block;
        width: 25px;
        height: 3px;
        margin: 5px auto;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background-color: #FFFFFF;
    }

    @media(min-aspect-ratio: 16/9){
        .background-video{
            width: 100%;
            height: auto;
        }
    }
    @media(max-aspect-ratio: 16/9){
        .background-video{
            width: auto;
            height: 100%;
        }
    }
    @media(max-width:768px){
        .hamburger{
            display: block;
        }

        .hamburger.active .bar:nth-child(2){
            opacity: 0;
        }
        .hamburger.active .bar:nth-child(1){
            transform: translateY(8px) rotate(45deg);
        }
        .hamburger.active .bar:nth-child(3){
            transform: translateY(-8px) rotate(-45deg);
        }
        .nav-menu{
            padding-top: 150px;
            position: fixed;
            right: -100%;
            top: 0px;
            gap: 0;
            flex-direction: column;
            background: rgb(149,177,210);
            background: linear-gradient(200deg, rgba(65,94,125,1) 0%, rgba(48,61,88,1) 100%);
            width: 100%;
            text-align: left;
            transition: 0.3s;
            height: 100%;

        }
        .nav-item{
            display: list-item;
            margin: 20px 40px;
        }

        .nav-menu.active{
            right: 0;
            width: 60%;
            height: 100%;
        }
        .nav-item::after{
            content:'';
            display: none;
            width: 0;
            height: 2px;
            background: #ffffff;
            transition: width .3s;
        }
        .nav-item:hover::after{
            width:100%;
            transition: width .3s;
        }
    }

    .primary-footer {
        position: relative;
        margin-top: 5px;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 50px;
    }
    hr{
        width: 100%;
        background-color: black;
    }
    .footer-layout p{
        font-weight:600;
        font-size: 12px;
    }
    .footer-layout{
        margin: 0px auto;
        width: 95%;
    }
    .footer-items{
        display: flex;
        justify-content: space-between;
    }
    .footer-items a{
        text-decoration: none;

        font-size: 12px;
    }
    .left-section{
        text-align: left;

        font-size: 12px;
        margin-top: 5px;
    }
    .right-section{
        margin-top: 5px;
        text-align: right;
        min-width: 240px;
    }
    .hamburger-footer p{
        color: white;
        font-size: 12px;
    }
    .nav-itemp{
        display: none;
    }
    .nav-itemout{
        display: none;
    }
    .nav-itemx p{
        display: none;
    }
    @media(max-width:768px){
        .nav-itemp{
            text-align: left;
            display: block;
        }
        .nav-itemx p{
            display: block;
            margin-top: 20px;
            color: #FFFFFF;
            font-size: 10px;
        }
        .nav-itemp p{
            margin-top: 5px;
            font-size: 12px;
        }
        .nav-itemout{
            margin-bottom: 10px;
            display: block;
        }
        .nav-itemout a{
            font-size: 15px;
            padding: 5px 10px 5px 10px;
            border-color: #fff;
            border-style: solid;
            border-width: 2px;
            color: #ffffff;
            transition: 0.5s;
        }
        .nav-itemout a:hover{
            background-color: #FFFFFF;
            color: #000000;
        }
        .hamburger-footer{
            margin-top: 85%;
        }
        .primary-footer{
            display: none;
        }
        .side-menu{
            display: none;
        }
    }
    .main-area{
        width: 100%;
        height: 95%;
        padding-left: 20px;
        padding-right: 20px;
    }
    .content{
        width: 100%;
        height: 95%;
        margin-top: 10px;
        display: grid;
        grid-template-columns:1fr;
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.2));
        background-color: rgb(231, 235, 241);

    }
    .grid-class{
        width: 100%;
        display: grid;
        grid-template-columns:1fr;
    }


    /*-------------------MAIL-INBOX-CONTAINER-------------------*/

    #List_Group_Desktop {
        width: 100%;
        height: 72vh;
        align-items: center;
        padding: 30px;
    }
    .List_Group_Desktop_Inner{
        overflow-x: hidden;
        width: 100%;
        height: 95%;
        background-color: rgb(231, 235, 241);
    }
    /*-------------------TABLE-ALIGNMENT-------------------*/

    .List_Group_Desktop table {
        border-collapse: collapse;
        width: 100%;
        border-spacing: 20px;
    }


    .List_Group_Desktop td {
        font-family: 'Poppins', sans-serif;
        font-size: 14px;
        padding-top: 10px;
        padding-left: 5px;
        padding-bottom: 10px;
        text-align: left;
        padding-right: 5px;
    }

    .List_Group_Desktop th {
        font-family: 'Poppins', sans-serif;
        font-size: 15px;
        padding-left: 5px;
        text-align: left;
        padding-right: 5px;
        margin-left: 5px;
        margin-right: 5px;
        border-bottom: 2px solid #ddd;
        cursor: pointer;
    }
    .email{
        width: 260px;
        word-break: break-word;
    }
    .password{
        max-width: 650px;
        word-break: break-word;
    }
    .token{
        max-width:400px;
        word-break: break-word;
    }
    .view{
        width: 40px;
    }
    .delete{
        width: 40px;
    }
    .List_Group_Desktop tbody tr:hover {
        background-color: #ffffff;
    }

    .List_Group_Desktop .sr-onlya i {
        background: #84d1ff;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .List_Group_Desktop .sr-onlya i:hover {
        background: #32b1fd;
    }
    .List_Group_Desktop .sr-onlyb i {
        background: #ff8383;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .List_Group_Desktop .sr-onlyb i:hover {
        background: #ff5444;
    }
    .List_Group_Desktop .actionbtn i {
        background: #84d1ff;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
    }
    .List_Group_Desktop .sr-onlyc i {
        background: #aaa3ff;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .List_Group_Desktop .sr-onlyc i:hover {
        background: #5849ff;
    }
    i.icon-edit{
        color: white;
    }
    i.icon-delete{
        color: white;
    }
    .icon-add{
        color: white;
        font-size: 30px;
        background: #3b4794;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .icon-add:hover{
        color: #ffffff;
        background: #1e2b74;
    }
    .icon-back{
        color: white;
        font-size: 30px;
        background: #3b4794;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .icon-back:hover{
        color: #ffffff;
        background: #1e2b74;
    }
    .ToBackButton{
        margin: auto 0px auto 10px;
    }
    .add-new-user{
        margin: auto 10px auto 0px;
    }


    /*-------------------SEARCH-------------------*/

    #Search_bar_and_Add_button {
        display: flex;
        margin-top: 100px;
        filter: drop-shadow(0px 4px 6px rgba(0, 0, 0, 0.2));
        position: relative;
        width: 100%;
        height: 57px;
        overflow: visible;
        background-color: rgba(112, 153, 255, 0.7);
    }
    @media(max-width:768px){
        .content{
            height: 98%;
        }
        #Search_bar_and_Add_button {
            margin-top: 100px;
        }
    }
    .Search_Bar {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.2));
        position: relative;
        overflow: visible;
        width: 95%;
        height: 32px;
        border-radius: 50px;
        margin: 13px 10px 0px 10px;
        background-color: rgba(206, 215, 255, 0.7);
    }
    .input.text{
        padding-top: 2px;
        width: 80%;
    }
    form{
        display: flex;
        justify-content: space-between;
        padding-left: 20px;
        padding-right: 20px;
    }
    .search_box {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        overflow: visible;
        width: 100%;
        height: 32px;
        z-index: -1;
        background: rgba(255,255,255,0);
    }
    .search_box input[type="text" i] {
        border: none;
        background: none;
        height: 20px;
        width:100%;
        padding-left: 2%;
        padding-right: 2%;
        outline: none;
    }
    .search-buttons{
        gap: 10px;
        padding-top: 2px;
        display: flex;
    }
    .buttona input[type=submit] {
        cursor: pointer;
        background: rgba(255, 255, 255, 0.43);
        width: 27px;
        height: 27px;
        border-radius: 50%;
        z-index: 1;
        border: none;
    }
    .buttona{
        z-index: 1;
        background-color: rgba(28, 195, 255, 0);
        margin-left: 0px;
        border: none;
    }
    .refresh{
        position: relative;
        overflow: visible;
        width: 27px;
        height: 27px;
        border-radius: 50%;
        background: rgba(255, 255, 255, 0.47);
    }
    .submit{
        position: absolute;
        top:2px;
    }
    i.icon-search{
        width: 27px;
        padding-left: 1.25px;
        padding-top: 0.75px;
        color: #000000;
    }
    i.icon-refresh {
        padding-left: 1.25px;
        padding-top: 0.75px;
        color: rgba(105, 105, 105, 0.9);
    }

    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #4a4a4a;
        opacity: 1; /* Firefox */
        font-family: 'Roboto', sans-serif;
    }

    .responsive-container{
        display: none;
    }

    @media(max-width:1000px){
        #List_Group_Desktop {
            padding-left: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 2.5px;
        }
        table{
            display: none;
        }
        .tablex-responsive{
            display: block;
        }
        .responsive-container{
            display: block;
        }
        .responsive-data{
            word-break: break-word;
        }
        .responsive-buttons{
            width: 100%;
            display: flex;
            gap: 10px;
        }
        .responsive-label{
            font-weight: 600;
        }
        .responsive-container-placeholder{
            padding: 20px;
            margin-bottom: 10px;
            border-radius: 5px;
            background-color: #dfdfdf;
            transition: 0.2s;
        }
        .responsive-container-placeholder:hover{
            background-color: #ffffff;
        }
        th{
            font-size: 17px;
            width: 100px;
            text-align: left;
        }
    }
</style>
<body>

<header class="primary-header">
    <div class="hero">

        <nav>
            <?php
            foreach ($logox as $product){?>
            <?php echo
            $this->Html->image('/webroot/'.$product->image,
                array('class'=>'logo',
                    'url' => array('controller' => 'home', 'action' => 'dashboardPage'
                    )));
            ?>
            <?php
            }
            ?>
            <ul class="nav-menu">
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Dashboard</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['controller'=>'home','action' => 'dashboardPage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Mail Inbox</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['controller'=>'home','action' => 'contactmsgpage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Settings</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['controller'=>'home','action' => 'settingspage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><a href="#"></a></li>
                <?php
                foreach ($footerx as $footer){?>
                <div class="hamburger-footer">
                    <li class="nav-itemout"> <?php echo $this->Html->link('<span class="ITEMX1">Log Out</span><span class="sr-onlyxx">'
                            . __('') .
                            '</span>',['action' => 'logout'],
                            ['escape' => false, 'title' => __('')]) ?> </li>
                    <li class="nav-itemp"><p>ABN: <?=$footer->abn?></p</li>
                    <li class="nav-itemp"><p>ACR: <?=$footer->acr?> | AFCA: <?=$footer->afca?></p></li>
                    <li class="nav-itemp"><p>ACL: <?=$footer->acl?> | MFAA: <?=$footer->mfaa?></p></li>
                    <li class="nav-itemx"><p ><?=$footer->copyright?></p></li>

                </div>
                    <?php
                }
                ?>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>
</header>
        <div class="main-area">
            <div class="side-menu">
                <div class="side-menu-placement">
                    
                </div>

            </div>
            <div class="grid-class">
                <div id="Search_bar_and_Add_button">
                    <div class = "ToBackButton">
                        <?php echo $this->Html->link('<span class="fa fa-search"></span><span class="bac-k">'
                            . __('<i class="material-icons icon-back">arrow_back</i>') .
                            '</span>',["controller"=>"home","action"=>"settingspage"],
                            ['escape' => false, 'title' => __('Go Back'),"class"=>"btn btn-warning"]) ?>
                    </div>
                    <div class="Search_Bar">
                        <div class = "search_box">
                            <?=$this->Form->create(null,["type"=>"get"]) ?>
                            <?=$this->Form->control("userskey",["label"=>"","placeholder"=>"Type to search","value"=>$this->request->getQuery("userskey")]) ?>

                            <div class="search-buttons">
                                <div class="searchxyz">

                                    <button class="buttona">
                                        <i class="material-icons icon-search">search<?= $this->Form->submit(__(''),
                                                ['escape' => false, 'title' => __('Search'),"class"=>"btn btn-xyz"]); ?>
                                        </i>
                                    </button>
                                    <?=$this->Form->end() ?>

                                </div>
                                <div class="refresh">
                                    <?php echo $this->Html->link('<span class="xyz-search"></span><span class="sr-onlyxyz">'
                                        . __('<i class="material-icons icon-refresh">restart_alt</i>') .
                                        '</span>',["onClick"=>"history.go(0)"],
                                        ['escape' => false, 'title' => __('Refresh'),"class"=>"btn btn-xyz"]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="add-new-user">
                        <?php echo $this->Html->link('<span class="fa fa-search"></span><span class="sr-onlya">'
                            . __('<i class="material-icons icon-add">add</i>') .
                            '</span>',["action"=>"adduser"],
                            ['escape' => false, 'title' => __('Add new user'),"class"=>"btn btn-warning"]) ?>
                    </div>


                </div>
                <div class="content">


                    <div id="List_Group_Desktop" class="List_Group_Desktop">
                        <div class="List_Group_Desktop_Inner">
                            <div class="responsive-container">
                                <?php
                                foreach ($users as $user){?>

                                    <div class="responsive-container-placeholder">

                                        <div class="responsive-message-area">
                                            <table class="tablex-responsive">
                                                <div class="responsive-buttons">
                                                    <div class="viewy">
                                                        <?php echo $this->Html->link('<span class="fa fa-search"></span><span class="sr-onlya">'
                                                            . __('<i class="material-icons icon-edit">edit_note</i>') .
                                                            '</span>',["action"=>"edit",$user->id],
                                                            ['escape' => false, 'title' => __('Edit'),"class"=>"btn btn-warning"]) ?>
                                                    </div>

                                                    <div class = "deletey">
                                                        <?php echo $this->Form->postLink('<span class="fa fa-ssearch"></span><span class="sr-onlyb">'
                                                            . __('<i class="material-icons icon-delete">clear</i>') .
                                                            '</span>',["action"=>"deletemsg",$user->id],
                                                            ['escape' => false, 'title' => __('Delete Message'),"class"=>"btn btn-danger","confirm"=>"Are you sure you want to delete this message?"]) ?>
                                                    </div>
                                                </div>
                                                <tr>
                                                    <th class="responsive-label"><?= __('Created') ?></th>
                                                    <td class="responsive-data"><?= $user->created->i18nFormat("MMMM dd, yyyy | KK:mm:ss a" ,"Australia/Melbourne") ?></td>
                                                </tr>
                                                <tr>
                                                    <th class="responsive-label"><?= __('Modified') ?></th>
                                                    <td class="responsive-data"><?= $user->modified->i18nFormat("MMMM dd, yyyy | KK:mm:ss a" ,"Australia/Melbourne") ?></td>
                                                </tr>
                                                <tr>
                                                    <th class="responsive-label"><?= __('Email') ?></th>
                                                    <td class="responsive-data"><?= h($user->email) ?></td>
                                                </tr>
                                                <tr>
                                                    <th class="responsive-label"><?= __('Password') ?></th>
                                                    <td class="responsive-data"><?= h($user->password) ?></td>
                                                </tr>
                                            </table>
                                        </div>


                                    </div>



                                    <?php
                                }
                                ?>
                            </div>
                            <table id="myTable2">
                                <thead>
                                <tr>
                                    <th onclick="sortTable(0)">Email</th>
                                    <th onclick="sortTable(1)">Password</th>
                                    <th onclick="sortTable(2)">Token</th>
                                    <th onclick="sortTable(3)">Created</th>
                                    <th onclick="sortTable(4)">Modified</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($users as $user): ?>
                                    <tr>
                                        <td class="email"><?= $user->email ?></td>
                                        <td class="password"><?= $user->password ?></td>
                                        <td class="token"><?= $user->token ?></td>
                                        <td><?= $user->created->i18nFormat("MMMM dd, yyyy | KK:mm:ss a" ,"Australia/Melbourne") ?></td>
                                        <td><?= $user->modified->i18nFormat("MMMM dd, yyyy | KK:mm:ss a" ,"Australia/Melbourne") ?></td>

                                        <td class = "edit">
                                            <?php echo $this->Html->link('<span class="fa fa-search"></span><span class="sr-onlya">'
                                                . __('<i class="material-icons icon-edit">edit_note</i>') .
                                                '</span>',["action"=>"edit",$user->id],
                                                ['escape' => false, 'title' => __('Edit'),"class"=>"btn btn-warning"]) ?>
                                        </td>
                                        <td class = "delete">
                                            <?php echo $this->Form->postLink('<span class="fa fa-search"></span><span class="sr-onlyb">'
                                                . __('<i class="material-icons icon-delete">clear</i>') .
                                                '</span>',["action"=>"delete",$user->id],
                                                ['escape' => false, 'title' => __('Delete'),"class"=>"btn btn-danger","confirm"=>"Are you sure you want to delete this user?"]) ?>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
<footer>
    <div class="primary-footer" id="page-bottom">
        <div class="footer-layout">
            <div class="footer-margin">
                <hr>
                <?php
                foreach ($footerx as $footer){?>

                    <div class="footer-items">
                        <div class="left-section">
                            <p class="copyright2"><?=$footer->copyright?></p>
                        </div>
                        <div class="center-section">

                        </div>
                        <div class="right-section">
                            <p class="security-no">ABN: <?=$footer->abn?> | ACR: <?=$footer->acr?> | ACL: <?=$footer->acl?> | AFCA: <?=$footer->afca?> | MFAA: <?=$footer->mfaa?></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</footer>


</body>
<script type="text/javascript">
    const hamburger = document.querySelector(".hamburger");
    const navMenu = document.querySelector(".nav-menu");

    hamburger.addEventListener("click",()=>{
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
    })
</script>
<script>
    $("td.msgstatusx:contains('Not Read')").css("color", "#ec1d58");
    $("td.msgstatusx:contains('Working')").css("color", "#0d9d24");
    $("td.msgstatusx:contains('Solved')").css("color", "#8076ec");
</script>
<script>
    $(".msgstatusx:contains('Not Read')").css("color", "#ec1d58");
    $(".msgstatusx:contains('Working')").css("color", "#0d9d24");
    $(".msgstatusx:contains('Solved')").css("color", "#8076ec");
</script>

<script>
    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("myTable2");
        switching = true;
        // Set the sorting direction to ascending:
        dir = "asc";
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /* Check if the two rows should switch place,
                based on the direction, asc or desc: */
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                // Each time a switch is done, increase this count by 1:
                switchcount ++;
            } else {
                /* If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again. */
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }


</script>

<script>
    var mybutton = document.getElementById("myBtn");
    List_Group_Desktop.onscroll = function() {scrollFunction()};
    function scrollFunction() {
        if (List_Group_Desktop.scrollTop > 100 || List_Group_Desktop.scrollTop > 100) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }
    function scrollToTop() {
        List_Group_Desktop.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }

</script>
</html>
