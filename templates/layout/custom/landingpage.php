<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <title>Home Loan Finance | Home</title>
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
</head>
<style>
    *{
        margin: 0%;
        padding: 0%;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
    }

    .hero{
        width: 100%;
        height: 100vh;
        background-image: linear-gradient(rgba(255, 255, 255, 0.0),rgba(255, 255, 255, 0.0));
        position: relative;
        display: flex;
        align-items: center;
    }
    nav{
        width: 100%;
        position: fixed;
        top: 0;
        left: 0;
        padding: 10px 8%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        transition: 0.6s;
        font-family: 'Poppins', sans-serif;
        z-index: +1;
    }

    nav .logo{
        position: relative;
        width: 130px;
        transition: 0.6s;
    }
    nav .logo:hover{
        width: 135px;
    }

    nav ul li {
        list-style: none;
        display: inline-block;
        margin-left: 40px;
        transition: 0.6s;
    }

    nav ul li a{
        text-decoration: none;
        color: #fff;
        font-size: 17px;
        transition: 0.6s;
    }
    .nav-item::after{
        content:'';
        display: block;
        width: 0;
        height: 2px;
        background: #ffffff;
        transition: width .3s;
    }
    .nav-item:hover::after{
        width:100%;
        transition: width .3s;
    }
    .nav-itema::after{
        content:'';
        display: block;
        height: 2px;
        background: #ffffff;
        width:100%;
    }
    .nav-menu{
        padding-top: 25px;
    }

    .hamburger{
        display: none;
        cursor: pointer;
    }

    .bar{
        display: block;
        width: 25px;
        height: 3px;
        margin: 5px auto;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background-color: #FFFFFF;
    }
    video{
        width: 100%;
        position: absolute;
        right: 0%;
        bottom: 0%;
        z-index: -1;
    }
    @media(min-aspect-ratio: 16/9){
        video{
            width: 100%;
            height: auto;
        }
    }
    @media(max-aspect-ratio: 16/9){
        video{
            width: auto;
            height: 100%;
        }
    }
    @media(max-width:850px){
        .hamburger{
            display: block;
        }

        .hamburger.active .bar:nth-child(2){
            opacity: 0;
        }
        .hamburger.active .bar:nth-child(1){
            transform: translateY(8px) rotate(45deg);
        }
        .hamburger.active .bar:nth-child(3){
            transform: translateY(-8px) rotate(-45deg);
        }
        .nav-menu{
            padding-top: 150px;
            position: fixed;
            right: -100%;
            top: 0px;
            gap: 0;
            flex-direction: column;
            background: rgb(149,177,210);
            background: linear-gradient(200deg, rgba(65,94,125,1) 0%, rgba(48,61,88,1) 100%);
            width: 100%;
            text-align: left;
            transition: 0.3s;
            height: 100%;

        }
        .nav-item{
            display: list-item;
            margin: 20px 40px;
        }

        .nav-menu.active{
            right: 0;
            width: 60%;
            height: 100%;
        }
        .nav-item::after{
            content:'';
            display: none;
            width: 0;
            height: 2px;
            background: #ffffff;
            transition: width .3s;
        }
        .nav-item:hover::after{
            width:100%;
            transition: width .3s;
        }
    }
    .content{
        position: absolute;
        top: 40%;
        width: 100%;
        text-align: left;
        padding: 0px 4%;
    }
    .content-inside{
        border-radius: 5px;
        padding: 25px 5% 50px;
    }
    .content h1{
        font-size: 54.5px;
        color: #fff;
        font-weight: 600;
        transition: 0.5s;
    }
    .content h2{
        font-size: 27.35px;
        color: #fff;
        font-weight: 600;
    }
    .button-area{
        margin-top: 30px;
    }
    .appointment-button a {
        font-weight: 600;
        border-radius: 5px;
        text-decoration: none;
        padding: 10px 25px 10px 25px;
        color: rgb(255, 255, 255);
        transition: 0.5s;
        border-style:solid;
        border-width: 3px;
        border-color: #FFFFFF;
    }
    .appointment-button a:hover {
        border-color: #ffffff;
        border-style:solid;
        border-width: 3px;
        background-color:rgb(255, 255, 255);
        color: rgb(0, 0, 0);
    }
    .primary-footer {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 50px;
    }
    hr{
        width: 100%;
    }
    .footer-layout p{
        font-size: 12px;
        color: #FFFFFF;
    }
    .footer-layout{
        margin: auto;
        width: 83.75%;
    }
    .footer-items{
        display: flex;
        justify-content: space-between;
    }
    .copyright2{
        display: none;
    }
    .footer-items a{
        text-decoration: none;
        color: #fff;
        font-size: 12px;
    }
    .left-section{
        text-align: left;
        color: #fff;
        font-size: 12px;
        margin-top: 5px;
    }
    .right-section{
        margin-top: 5px;
        text-align: right;
        min-width: 240px;
    }
    .hamburger-footer p{
        color: white;
        font-size: 12px;
    }
    .nav-itemp{
        display: none;
    }
    .nav-itemx p{
        display: none;
    }
    @media(max-width:850px){
        .nav-itemp{
            text-align: left;
            display: block;
        }
        .nav-itemx p{
            display: block;
            margin-top: 20px;
            color: #FFFFFF;
            font-size: 10px;
        }
        .nav-itemp p{
            margin-top: 5px;
            font-size: 12px;
        }
        .hamburger-footer{
            margin-top: 85%;
        }
        .primary-footer{
            display: none;
        }
        .content h1{
            font-size: 35px;
        }
        .content h2{
            font-size: 17.35px;
        }
        .appointment-button a {
            padding: 7.5px 17px 7.5px 17px;
        }
        .content h1:hover{
        -webkit-text-stroke: 2px #fff;
        color: transparent;
    }
    }
    @media(max-width:400px){
        .breaktext{
            display: none;
        }
    }
    @media(max-width:1010px){
        .copyright2{
            display: block;
        }
        .emailandphone{
            display: none;
        }
        .address1{
            display: none;
        }
        .copyright1{
            display: none;
        }
    }

</style>
<body>

<header class="primary-header">
    <div class="hero">

        <?= $this->Html->media(
            [['src' => '/webroot/video/homevideolight.mp4', 'type' => "video/mp4"]],
            ['autoplay','loop','muted','plays-inline']
        ) ?>


        <div class="primary-footer">
            <div class="footer-layout">
                <hr>
                <?php
                foreach ($footerx as $footer){?>

                <div class="footer-items">
                    <div class="left-section">

                        <span class="emailandphone">Email: <?=$footer->email?> | Phone: <?=$footer->phone?></span>
                        <p class="address1">Address: <?=$footer->address?></p>
                        <p class="copyright2"><?=$footer->copyright?></p>
                    </div>
                    <div class="center-section">

                    </div>
                    <div class="right-section">
                        <p class="security-no">ABN: <?=$footer->abn?> | ACR: <?=$footer->acr?> | ACL: <?=$footer->acl?> | AFCA: <?=$footer->afca?> | MFAA: <?=$footer->mfaa?></p>
                        <p class="copyright1"><?=$footer->copyright?></p>
                    </div>
                </div>
                <?php
                }
                ?>
                <div>
                </div>
            </div>
        </div>

        <nav>

            <?php
            foreach ($logox as $product){?>
                <?php echo
                $this->Html->image('/webroot/'.$product->image,
                    array('class'=>'logo',
                        'url' => array('controller' => 'home', 'action' => 'landingpage'
                        )));
                ?>
                <?php
            }
            ?>

            <ul class="nav-menu">
                <li class="nav-itema"> <?php echo $this->Html->link('<span class="ITEMX1">HOME</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"landingpage"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <li class="nav-item"> <?php echo $this->Html->link('<span class="ITEMX1">SERVICES</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"services"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <li class="nav-item"> <?php echo $this->Html->link('<span class="ITEMX1">CALCULATOR</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"calculatorone"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <li class="nav-item"> <?php echo $this->Html->link('<span class="ITEMX1">CONTACT</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',["action"=>"contact"],
                        ['escape' => false, 'title' => __('')]) ?> </li>
                <?php
                foreach ($footerx as $footer){?>
                <div class="hamburger-footer">

                    <li class="nav-itemp"><p>ABN: <?=$footer->abn?></p</li>
                    <li class="nav-itemp"><p>ACR: <?=$footer->acr?> | AFCA: <?=$footer->afca?></p></li>
                    <li class="nav-itemp"><p>ACL: <?=$footer->acl?> | MFAA: <?=$footer->mfaa?></p></li>
                    <li class="nav-itemx"><p ><?=$footer->copyright?></p></li>
                </div>
                    <?php
                }
                ?>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>
        <div class="content">
            <div class="content-inside">
                <div class="headline-text">
                    <h1>HOME LOAN FINANCE</h1>
                    <h2>Secure your future with endless promise
                    <br class="breaktext">We're always here to help</h2>
                </div>
                <div class="button-area">
                    <div class="appointment-button"><a <?php echo $this->Html->link('<span class="ITEMX1">BOOK AN APPOINTMENT</span><span class="sr-onlyxx">'
                            . __('') .
                            '</span>',["action"=>"contact"],
                            ['escape' => false, 'title' => __('')]) ?> </a></div>
                </div>
            </div>
        </div>



    </div>


</header>


</body>

<script type="text/javascript">
    const hamburger = document.querySelector(".hamburger");
    const navMenu = document.querySelector(".nav-menu");

    hamburger.addEventListener("click",()=>{
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
    })
</script>
</html>
