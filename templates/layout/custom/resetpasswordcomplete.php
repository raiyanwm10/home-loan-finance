<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />
    <title>Home Loan Finance | Reset Password Request</title>

    <style>
        * {
            margin: 0%;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Poppins', sans-serif;

        }

        .allbuttons {
            text-align: center;
            margin-top: 25px;
            width: 100%;
        }

        .allbuttons a {
            text-decoration: none;
        }

        .List_Group_Desktop {
            background: rgb(136,134,193);
            background: linear-gradient(90deg, rgba(136,134,193,1) 0%, rgba(110,123,200,0) 50%, rgba(67,111,133,1) 100%);
            margin: 150px auto;
            position: relative;
            width: 100%;
            box-shadow: black;
            align-items: center;
        }

        .login-body{
            margin: auto auto;
            padding: 2% 4%;
            height: 100%;
            width: 90%;
            max-width: 1000px;
            background-color: #ffffff;
            box-shadow: black
        }
        .logoimg{
            width: 250px;
        }

        h3{
            margin-top: 20px;
            word-break: break-word;
            font-size: 25px;
        }
        @media(max-width:768px){
            .login-body{
                height: 100%;
                width: 90%;
            }
        }
        .sr-onlyc i {
            background: #0E2F40FF;
            padding: 20px;
            border-radius: 50%;
            box-shadow: 0 3px 5px rgba(0,0,0,0.3);
            border: none;
            transition: 0.4s;
        }
        .sr-onlyc i:hover {
            background: #d8d6ff;
        }
        .icon-arrow-back{
            color: white;
            font-size: 30px;
        }
        .icon-arrow-back:hover{
            color: #000000;
        }
        .login-body{
            text-align: center;
        }


    </style>

</head>

<body>

<div class="loginpage">



    <div class="List_Group_Desktop">

        <div class="login-body">


            <div class="login">
                <img src="<?php echo APPLICATION_URL ?>/webroot/logos/Logowide2.png" class="logoimg">
                <h3>Password reset completed</h3>
                <h3>Please head back to login page and sign in with your new password.</h3>
            </div>



            <div class="allbuttons">

                <div class="view">
                    <?php echo $this->Html->link('<span class="fa fa-search"></span><span class="sr-onlyc">'
                        . __('<i class="material-icons icon-arrow-back">arrow_back_ios_new</i>') .
                        '</span>',["controller"=>"users","action"=>"login"],
                        ['escape' => false, 'title' => __('Go Back to Login Page'),"class"=>"btn btn-view2"]) ?>
                </div>
            </div>


        </div>
    </div>

</div>


</body>

</html>

