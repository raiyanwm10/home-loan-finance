<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <title>Home Loan Finance | Dashboard</title>
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
</head>
<style>
    *{
        margin: 0%;
        padding: 0%;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
    }
    ::-webkit-scrollbar{
        width: 15px;

    }
    ::-webkit-scrollbar-track {

    }
    ::-webkit-scrollbar-thumb{
        background: #ffffff;
        border-radius: 10px;

        box-shadow: inset 0 0 10px 10px #86a8fd;
        border: solid 3px transparent;
    }


    .hero{
        width: 100%;
        height: 5vh;
        background-image: linear-gradient(rgba(255, 255, 255, 0.0),rgba(255, 255, 255, 0.0));
        position: relative;
        display: flex;
        align-items: center;
        background-color: rgb(255, 255, 255);
    }
    nav{
        width: 100%;
        background: rgb(149,177,210);
        background: linear-gradient(90deg, rgba(149,177,210,1) 0%, rgba(65,94,125,1) 100%);
        position: fixed;
        top: 0;
        left: 0;
        padding: 10px 4%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        transition: 0.6s;
        font-family: 'Poppins', sans-serif;
        z-index: +1;
    }

    nav .logo{
        position: relative;
        width: 130px;
        transition: 0.6s;
    }

    nav ul li {
        list-style: none;
        display: inline-block;
        margin-left: 40px;
        transition: 0.6s;
    }

    nav ul li a{
        text-decoration: none;
        color: #fff;
        font-size: 17px;
        transition: 0.6s;
    }
    .nav-item{
        display: none;
    }
    .nav-item::after{
        content:'';
        display: block;
        width: 0;
        height: 2px;
        background: #ffffff;
        transition: width .3s;
    }
    .nav-item:hover::after{
        width:100%;
        transition: width .3s;
    }
    .nav-itema::after{
        content:'';
        display: block;
        height: 2px;
        background: #ffffff;
        width:100%;
    }
    .nav-menu{
        padding-top: 25px;
    }

    .hamburger{
        display: none;
        cursor: pointer;
    }

    .bar{
        display: block;
        width: 25px;
        height: 3px;
        margin: 5px auto;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background-color: #FFFFFF;
    }

    @media(min-aspect-ratio: 16/9){
        .background-video{
            width: 100%;
            height: auto;
        }
    }
    @media(max-aspect-ratio: 16/9){
        .background-video{
            width: auto;
            height: 100%;
        }
    }
    @media(max-width:768px){
        .hamburger{
            display: block;
        }

        .hamburger.active .bar:nth-child(2){
            opacity: 0;
        }
        .hamburger.active .bar:nth-child(1){
            transform: translateY(8px) rotate(45deg);
        }
        .hamburger.active .bar:nth-child(3){
            transform: translateY(-8px) rotate(-45deg);
        }
        .nav-menu{
            padding-top: 150px;
            position: fixed;
            right: -100%;
            top: 0px;
            gap: 0;
            flex-direction: column;
            background: rgb(149,177,210);
            background: linear-gradient(200deg, rgba(65,94,125,1) 0%, rgba(48,61,88,1) 100%);
            width: 100%;
            text-align: left;
            transition: 0.3s;
            height: 100%;

        }
        .nav-item{
            display: list-item;
            margin: 20px 40px;
        }

        .nav-menu.active{
            right: 0;
            width: 60%;
            height: 100%;
        }
        .nav-item::after{
            content:'';
            display: none;
            width: 0;
            height: 2px;
            background: #ffffff;
            transition: width .3s;
        }
        .nav-item:hover::after{
            width:100%;
            transition: width .3s;
        }
    }
    .main-area{
        margin-top: 85px;
        display: flex;
        width: 100%;
        height: 95%;
        min-height: 760px;
        padding-left: 20px;
        padding-right: 20px;
    }
    .content{
        gap: 20px;
        padding: 20px;
        width: 100%;
        height: 100%;
        margin-top: 10px;
        display: grid;
        grid-template-columns:2fr 1fr;
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.2));
        background-color: rgb(231, 235, 241);
    }

    .primary-footer {
        position: relative;
        margin-top: 25px;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 50px;
    }
   hr{
        width: 100%;
        background-color: black;
    }
    .footer-layout p{
        font-weight:600;
        font-size: 12px;
    }
    .footer-layout{
        margin: 0px auto;
        width: 95%;
    }
    .footer-items{
        display: flex;
        justify-content: space-between;
    }
    .footer-items a{
        text-decoration: none;

        font-size: 12px;
    }
    .left-section{
        text-align: left;

        font-size: 12px;
        margin-top: 5px;
    }
    .right-section{
        margin-top: 5px;
        text-align: right;
        min-width: 240px;
    }
    .hamburger-footer p{
        color: white;
        font-size: 12px;
    }
    .nav-itemp{
        display: none;
    }
    .nav-itemout{
        display: none;
    }
    .nav-itemx p{
        display: none;
    }
    @media(max-width:768px){
        .nav-itemp{
            text-align: left;
            display: block;
        }
        .nav-itemx p{
            display: block;
            margin-top: 20px;
            color: #FFFFFF;
            font-size: 10px;
        }
        .nav-itemp p{
            margin-top: 5px;
            font-size: 12px;
        }
        .nav-itemout{
            margin-bottom: 10px;
            display: block;
        }
        .nav-itemout a{
            font-size: 15px;
            padding: 5px 10px 5px 10px;
            border-color: #fff;
            border-style: solid;
            border-width: 2px;
            color: #ffffff;
            transition: 0.5s;
        }
        .nav-itemout a:hover{
            background-color: #FFFFFF;
            color: #000000;
        }
        .hamburger-footer{
            margin-top: 85%;
        }
        .primary-footer{
            display: none;
        }
        .side-menu{
            display: none;
        }
        .content{

        }
    }
    .content-item1{
        max-height: 200px;
        padding: 20px;
        background-color: white;
    }
    .content-item2{
        padding: 20px 10px 20px 20px;
        background-color: white;
        max-height: 580px;
    }
    .responsive-container{
        overflow: scroll;
        max-height: 510px;
        overflow-x: hidden;
    }
    .responsive-data{
        word-break: break-word;
    }
    .responsive-buttons{
        width: 100%;
        display: flex;
        gap: 10px;
    }
    .responsive-label{
        font-weight: 600;
    }
    .responsive-container-placeholder{
        padding: 20px;
        margin-bottom: 10px;
        border-radius: 5px;
        background-color: #dfdfdf;
        transition: 0.2s;
    }
    .responsive-container-placeholder:hover{
        background-color: #cacaca;
    }
    th{
        font-size: 17px;
        width: 100px;
        text-align: left;
    }
    .welcomedsh{
        padding: 30px 20px 10px 20px;
        text-align: left;
    }

    @media(max-width:1250px){
        .content{
            height: 98%;
            display: block;
        }
        .welcomedsh{
            padding: 30px 0px 10px 0px;
            text-align: left;
        }
    }
    .grid-class{
        width: 100%;
        display: grid;
        grid-template-columns:1fr;
    }
    .side-menu{
        min-width: 250px;
        background-color: rgb(255, 255, 255);
    }
    .side-menu span{
        font-family: 'Roboto', sans-serif;
    }
    .side-menu-placement{
        width: 100%;
        height: 300px;
        margin-top: 5px;
    }

    /* ------------------Side Bar Work------------------ */


    .side-menu-item1{
        background-color: #7f88ef;
        overflow: visible;
        width: 200px;
        height: 43px;
        position: absolute;
        left: 33px;
        top: 148px;
        border-radius: 6px;

    }

    /* ------------------ICONS------------------ */

    #Home_icon {
        text-align: center;
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        width: 38px;
        height: 35px;
        left: 39px;
        top: 152px;
        overflow: visible;
    }

    #SupplierInfo_icon {
        text-align: center;
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        width: 38px;
        height: 35px;
        left: 39px;
        top: 206px;
        overflow: visible;
    }
    #Message_icon {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        width: 38px;
        height: 35px;
        left: 39px;
        top: 260px;
        overflow: visible;
        text-align: center ;
    }
    #Setting_icon {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        width: 38px;
        height: 35px;
        left: 39px;
        top: 315px;
        overflow: visible;
        text-align: center ;
    }

    i.icon-home {
        margin-top: 4px;
        padding: 2px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border-radius: 6px;
        color: #ffffff;
        background-color: #e61b60;
        font-size: 24px;
    }
    i.icon-supplier{
        margin-top: 4px;
        padding: 2px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border-radius: 6px;
        color: #2a467b;
        background-color: #ffffff;
    }
    i.icon-setting{
        margin-top: 1px;
        padding: 2px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border-radius: 6px;
        color: #2a467b;
        background-color: #ffffff;
    }
    i.icon-message{
        margin-top: 1px;
        padding: 2px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border-radius: 6px;
        color: #2a467b;
        background-color: #ffffff;
    }


    /* ------------------ICONS-HOVER------------------ */

    a.sidebuttonhover{
        padding-left: 100px;
        padding-right: 100px;
        padding-bottom: 20px;
        border-radius: 6px;
        transition: 0.4s;
    }
    a.sidebuttonhover:hover{
        background: rgba(121, 121, 121, 0.13);
    }

    /* ------------------ICONS-TEXT------------------ */

    #Dashboard {
        left: 90px;
        top: 164px;
        position: absolute;
        overflow: visible;
        width: 64px;
        white-space: nowrap;
        line-height: 20px;
        margin-top: -3.5px;
        text-align: left;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        color: rgba(32, 33, 36, 1);
    }
    #Dashboard span{
        color: white;
    }

    #Supplier_Info {
        left: 90px;
        top: 216px;
        position: absolute;
        overflow: visible;
        width: 73px;
        white-space: nowrap;
        line-height: 20px;
        margin-top: -3.5px;
        text-align: left;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        color: rgba(32, 33, 36, 1);
    }
    #Message {
        left: 90px;
        top: 270px;
        position: absolute;
        overflow: visible;
        width: 54px;
        white-space: nowrap;
        line-height: 20px;
        margin-top: -3.5px;
        text-align: left;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        color: rgba(32, 33, 36, 1);
    }
    #Setting {
        left: 90px;
        top: 326px;
        position: absolute;
        overflow: visible;
        width: 54px;
        white-space: nowrap;
        line-height: 20px;
        margin-top: -3.5px;
        text-align: left;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        color: rgba(32, 33, 36, 1);
    }

    /* ------------------ICONS-BUTTONS------------------ */

    #Dashboard_Button {
        opacity: 0;
        fill: rgba(169, 180, 254, 1);
    }

    .Dashboard_Button {
        position: absolute;
        overflow: visible;
        width: 200px;
        height: 43px;
        left: 33px;
        top: 148px;
    }

    .Supplier_Info_Button {
        position: absolute;
        overflow: visible;
        width: 200px;
        height: 43px;
        left: 33px;
        top: 200px;
        cursor: pointer;
    }
    .Setting_Button{
        position: absolute;
        overflow: visible;
        width: 194px;
        height: 43px;
        left: 33px;
        top: 306px;
        cursor: pointer;
    }
    .Message_Button{
        position: absolute;
        overflow: visible;
        width: 194px;
        height: 43px;
        left: 33px;
        top: 252px;
        cursor: pointer;
    }
    #Logout_and_Profile_Layout {
        position: relative;
        width: 229px;
        height: 243px;
        top: 200px;
        overflow: visible;
    }
    #Logout_and_Profile_Layout span{
        font-family: 'Roboto', sans-serif;
    }
    .Rectangle_81_ed {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        overflow: visible;
        width: 36px;
        height: 36px;
        left: 10px;
        top: 4px;
        border-radius: 6px;
        text-align: center;
    }
    i.icon-logout{
        margin-top: 4px;
        padding: 2px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border-radius: 6px;
        color: #000000;
        background-color: rgba(234, 249, 255, 0.73);
    }

    .Log_out_Button {
        position: absolute;
        overflow: visible;
        width: 200px;
        height: 43px;
    }
    #Log_out {
        position: absolute;
        width: 207px;
        height: 47px;
        left: 12px;
        top: 77px;
        overflow: visible;
        transition: all 0.30000001192092896s ease-out;
        --web-animation: fadein 0.30000001192092896s ease-out;
        --web-action-type: page;
        --web-action-target: logout;
        cursor: pointer;
    }
    a.logoutbuttonhover{
        padding-left: 100px;
        padding-right: 100px;
        padding-bottom: 20px;
        border-radius: 6px;
        transition: 0.4s;
    }
    a.logoutbuttonhover:hover{
        background: rgba(220, 220, 220, 0.28);
    }
    .Bottom_section {
        filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
        position: absolute;
        overflow: visible;
        width: 200px;
        height: 44px;
        left: 15px;
        top: 77px;
        background: rgba(70, 117, 174, 0.44);
        border-radius: 8px;
    }
    #Log_Out span{
        color: white;
    }
    #Log_Out {
        left: 78px;
        top: 17px;
        position: absolute;
        overflow: visible;
        width: 47px;
        white-space: nowrap;
        text-align: center;
        font-family: Acumin Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        color: rgba(0, 0, 0, 1);
    }
    .recent-messages{
    }

    .sr-onlyb i {
        background: #ff8383;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .sr-onlyb i:hover {
        background: #ff5444;
    }
    .sr-onlyc i {
        background: #5e92ef;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        transition: .4s;
    }
    .sr-onlyc i:hover {
        background: #3b6bd1;
    }
    i.icon-edit{
        color: white;
    }
    i.icon-delete{
        color: white;
    }



</style>
<body>

<header class="primary-header">
    <div class="hero">

        <nav>
            <?php
            foreach ($logox as $product){?>
            <?php echo
            $this->Html->image('/webroot/'.$product->image,
                array('class'=>'logo',
                    'url' => array('controller' => 'home', 'action' => 'dashboardPage'
                    )));
            ?>
            <?php
            }
            ?>
            <ul class="nav-menu">
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Dashboard</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['action' => 'dashboardPage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Mail Inbox</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['action' => 'contactmsgpage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Settings</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['action' => 'settingspage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><a href="#"></a></li>
                <?php
                foreach ($footerx as $footer){?>
                <div class="hamburger-footer">
                    <li class="nav-itemout"> <?php echo $this->Html->link('<span class="ITEMX1">Log Out</span><span class="sr-onlyxx">'
                            . __('') .
                            '</span>',['action' => 'logout'],
                            ['escape' => false, 'title' => __('')]) ?> </li>
                    <li class="nav-itemp"><p>ABN: <?=$footer->abn?></p</li>
                    <li class="nav-itemp"><p>ACR: <?=$footer->acr?> | AFCA: <?=$footer->afca?></p></li>
                    <li class="nav-itemp"><p>ACL: <?=$footer->acl?> | MFAA: <?=$footer->mfaa?></p></li>
                    <li class="nav-itemx"><p ><?=$footer->copyright?></p></li>

                </div>
                    <?php
                }
                ?>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>
 </header>
 
        <div class="main-area">
            <div class="side-menu">
                <div class="side-menu-placement">

                    <div class="list-button">
                        <div class="side-menu-item1">

                        </div>
                        <div class="side-menu-item2">

                        </div>
                        <div class="side-menu-item3">

                        </div>
                        <div class="side-menu-item4">

                        </div>
                    </div>

                    <div id="Home_icon">
                        <i class="material-icons icon-home">home</i>
                    </div>
                    <div id="SupplierInfo_icon">
                        <i class="material-icons icon-message">mail</i>
                    </div>
                    <div id="Message_icon">
                        <i class="material-icons icon-setting">settings</i>
                    </div>
                    <div id="Setting_icon">

                    </div>
                    <div id="Dashboard">
                        <span>Dashboard</span>
                    </div>
                    <div id="Supplier_Info">
                        <span>Mail Inbox</span>
                    </div>
                    <div id="Message">
                        <span>Settings</span>
                    </div>
                    <div id="Setting">

                    </div>
                    <div class="Dashboard_Button">
                        <rect id="Dashboard_Button" rx="10" ry="10" x="0" y="0" width="200" height="43">
                        </rect>
                    </div>
                    <div class="Supplier_Info_Button">
                        <?= $this->Html->link(__(''), ['action' => 'contactmsgpage'], ['class' => 'sidebuttonhover']) ?>
                    </div>
                    <div class="Message_Button">
                        <?= $this->Html->link(__(''), ['action' => 'settingspage'], ['class' => 'sidebuttonhover']) ?>
                    </div>

                </div>

                <div id="Logout_and_Profile_Layout">
                    <div class="Bottom_section">

                    </div>

                    <div onclick="application.goToTargetView(event)" id="Log_out">
                        <!-- <div onclick="application.logout(event)"> -->
                        <div class="Rectangle_81_ed">
                            <i class="material-icons icon-logout">logout</i>
                        </div>
                        <div id="Log_Out">
                            <span>Log Out</span>
                        </div>

                        <div class="Log_out_Button">
                            <?= $this->Html->link(__(''), ['action' => 'logout'], ['class' => 'logoutbuttonhover']) ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="grid-class">
                <div class="content">

                    <div class="content-item1">
                        <div class="welcomedsh">
                            <h1>WELCOME<br>TO THE DASHBOARD</h1>
                        </div>
                    </div>
                    <div class="content-item2">
                        <h3>Recent Messages</h3>
                        <div class="responsive-container">
                            <?php
                            foreach ($messages as $msg_info){?>

                                <div class="responsive-container-placeholder">

                                    <div class="responsive-message-area">
                                        <table class="tablex-responsive">
                                            <div class="responsive-buttons">
                                                <div class="viewy">
                                                    <?php echo $this->Html->link('<span class="fa fa-search"></span><span class="sr-onlyc">'
                                                        . __('<i class="material-icons icon-edit">description</i>') .
                                                        '</span>',["action"=>"messageview",$msg_info->id],
                                                        ['escape' => false, 'title' => __('View Message'),"class"=>"btn btn-view2"]) ?>
                                                </div>

                                                <div class = "deletey">
                                                    <?php echo $this->Form->postLink('<span class="fa fa-search"></span><span class="sr-onlyb">'
                                                        . __('<i class="material-icons icon-delete">clear</i>') .
                                                        '</span>',["action"=>"deletemsgb",$msg_info->id],
                                                        ['escape' => false, 'title' => __('Delete Message'),"class"=>"btn btn-danger","confirm"=>"Are you sure you want to delete this message?"]) ?>
                                                </div>
                                            </div>
                                            <tr>
                                                <th class="responsive-label"><?= __('Received') ?></th>
                                                <td class="responsive-data">
                                                    <?=$msg_info->created->i18nFormat("MMMM dd, yyyy | KK:mm:ss a" ,"Australia/Melbourne") ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="responsive-label"><?= __('First Name') ?></th>
                                                <td class="responsive-data"><?= h($msg_info->first_name) ?></td>
                                            </tr>
                                            <tr>
                                                <th class="responsive-label"><?= __('Service') ?></th>
                                                <td class="responsive-data"><?= h($msg_info->subject) ?></td>
                                            </tr>
                                        </table>
                                    </div>


                                </div>



                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <footer>
    <div class="primary-footer" id="page-bottom">
        <div class="footer-layout">
            <div class="footer-margin">
                <hr>
                <?php
                foreach ($footerx as $footer){?>

                    <div class="footer-items">
                        <div class="left-section">
                            <p class="copyright2"><?=$footer->copyright?></p>
                        </div>
                        <div class="center-section">

                        </div>
                        <div class="right-section">
                            <p class="security-no">ABN: <?=$footer->abn?> | ACR: <?=$footer->acr?> | ACL: <?=$footer->acl?> | AFCA: <?=$footer->afca?> | MFAA: <?=$footer->mfaa?></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</footer>



</body>
<script type="text/javascript">
    window.addEventListener("scroll",function(){
        var nav = document.querySelector("nav");
        nav.classList.toggle("sticky",window.scrollY>0)
    })
</script>
<script type="text/javascript">
    const hamburger = document.querySelector(".hamburger");
    const navMenu = document.querySelector(".nav-menu");

    hamburger.addEventListener("click",()=>{
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
    })
</script>
</html>
