<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <title>Home Loan Finance | Update User</title>
    <link rel="icon" href="<?php echo APPLICATION_URL ?>/webroot/icons/fav3.png" sizes="16x16 32x32" type="image/png">
</head>
<style>
    *{
        margin: 0%;
        padding: 0%;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
    }

    .hero{
        width: 100%;
        height: 5vh;
        background-image: linear-gradient(rgba(255, 255, 255, 0.0),rgba(255, 255, 255, 0.0));
        position: relative;
        display: flex;
        align-items: center;
        background-color: rgb(255, 255, 255);
    }
    nav{
        width: 100%;
        background: rgb(149,177,210);
        background: linear-gradient(90deg, rgba(149,177,210,1) 0%, rgba(65,94,125,1) 100%);
        position: fixed;
        top: 0;
        left: 0;
        padding: 10px 4%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        transition: 0.6s;
        font-family: 'Poppins', sans-serif;
        z-index: +1;
    }

    nav .logo{
        position: relative;
        width: 130px;
        transition: 0.6s;
    }

    nav ul li {
        list-style: none;
        display: inline-block;
        margin-left: 40px;
        transition: 0.6s;
    }

    nav ul li a{
        text-decoration: none;
        color: #fff;
        font-size: 17px;
        transition: 0.6s;
    }
    .nav-item{
        display: none;
    }
    .nav-item::after{
        content:'';
        display: block;
        width: 0;
        height: 2px;
        background: #ffffff;
        transition: width .3s;
    }
    .nav-item:hover::after{
        width:100%;
        transition: width .3s;
    }
    .nav-itema::after{
        content:'';
        display: block;
        height: 2px;
        background: #ffffff;
        width:100%;
    }
    .nav-menu{
        padding-top: 25px;
    }

    .hamburger{
        display: none;
        cursor: pointer;
    }

    .bar{
        display: block;
        width: 25px;
        height: 3px;
        margin: 5px auto;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background-color: #FFFFFF;
    }

    @media(min-aspect-ratio: 16/9){
        .background-video{
            width: 100%;
            height: auto;
        }
    }
    @media(max-aspect-ratio: 16/9){
        .background-video{
            width: auto;
            height: 100%;
        }
    }
    @media(max-width:768px){
        .hamburger{
            display: block;
        }

        .hamburger.active .bar:nth-child(2){
            opacity: 0;
        }
        .hamburger.active .bar:nth-child(1){
            transform: translateY(8px) rotate(45deg);
        }
        .hamburger.active .bar:nth-child(3){
            transform: translateY(-8px) rotate(-45deg);
        }
        .nav-menu{
            padding-top: 150px;
            position: fixed;
            right: -100%;
            top: 0px;
            gap: 0;
            flex-direction: column;
            background: rgb(149,177,210);
            background: linear-gradient(200deg, rgba(65,94,125,1) 0%, rgba(48,61,88,1) 100%);
            width: 100%;
            text-align: left;
            transition: 0.3s;
            height: 100%;

        }
        .nav-item{
            display: list-item;
            margin: 20px 40px;
        }

        .nav-menu.active{
            right: 0;
            width: 60%;
            height: 100%;
        }
        .nav-item::after{
            content:'';
            display: none;
            width: 0;
            height: 2px;
            background: #ffffff;
            transition: width .3s;
        }
        .nav-item:hover::after{
            width:100%;
            transition: width .3s;
        }
    }

    .primary-footer {
        position: relative;
        margin-top: 25px;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 50px;
    }
    hr{
        width: 100%;
        background-color: black;
    }
    .footer-layout p{
        font-weight:600;
        font-size: 12px;
    }
    .footer-layout{
        margin: 0px auto;
        width: 95%;
    }
    .footer-items{
        display: flex;
        justify-content: space-between;
    }
    .footer-items a{
        text-decoration: none;

        font-size: 12px;
    }
    .left-section{
        text-align: left;

        font-size: 12px;
        margin-top: 5px;
    }
    .right-section{
        margin-top: 5px;
        text-align: right;
        min-width: 240px;
    }
    .hamburger-footer p{
        color: white;
        font-size: 12px;
    }
    .nav-itemp{
        display: none;
    }
    .nav-itemout{
        display: none;
    }
    .nav-itemx p{
        display: none;
    }
    .main-area{
        width: 100%;
        height: 95%;
    }
    .content{
        margin: 50px auto;
        width: 90%;
        min-height: 720px;
    }
    @media(max-width:768px){
        .nav-itemp{
            text-align: left;
            display: block;
        }
        .nav-itemx p{
            display: block;
            margin-top: 20px;
            color: #FFFFFF;
            font-size: 10px;
        }
        .nav-itemp p{
            margin-top: 5px;
            font-size: 12px;
        }
        .nav-itemout{
            margin-bottom: 10px;
            display: block;
        }
        .nav-itemout a{
            font-size: 15px;
            padding: 5px 10px 5px 10px;
            border-color: #fff;
            border-style: solid;
            border-width: 2px;
            color: #ffffff;
            transition: 0.5s;
        }
        .nav-itemout a:hover{
            background-color: #FFFFFF;
            color: #000000;
        }
        .hamburger-footer{
            margin-top: 85%;
        }
        .primary-footer{
            display: none;
        }
        .content{
            min-height: 0px;
        }
    }
    .edit-contact-detail{
        margin-top: 100px;
    }
    .List_Group_Desktop table {
        border-collapse: collapse;
        width: 100%;
        border-spacing: 30px;
    }


    .List_Group_Desktop td {
        font-family: 'Poppins', sans-serif;
        font-size: 14px;
        padding-top: 15px;
        padding-left: 5px;
        padding-bottom: 15px;
        text-align: left;

    }

    .List_Group_Desktop th {
        font-family: 'Poppins', sans-serif;
        font-size: 15px;
        padding-left: 5px;
        text-align: left;
        border-bottom: 2px solid #ddd;
        cursor: pointer;
    }

    .List_Group_Desktop tbody tr:hover {
        background-color: #dff0fd;

    }

    .List_Group_Desktop table a .sr-onlya i {

        background: #84d1ff;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;

    }
    .List_Group_Desktop table a .sr-onlya i:hover {

        background: #32b1fd;


    }
    .List_Group_Desktop table a .sr-onlyb i {

        background: #ff8383;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;

    }
    .List_Group_Desktop table a .sr-onlyb i:hover {

        background: #ff5444;

    }

    .List_Group_Desktop table .actionbtn i {

        background: #84d1ff;
        padding: 4px;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;

    }
    input[type=text], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=password], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=number], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=float], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }
    input[type=email], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }

    label {
        padding: 12px 12px 12px 0;
        display: inline-block;
        font-weight: 600;
    }
    .List_Group_Desktop form {

        border-collapse: collapse;
        width: 100%;
        font-family: 'Poppins', sans-serif;

    }
    textarea{
        font-family: 'Poppins', sans-serif;

    }
    Input{
        font-family: 'Poppins', sans-serif;
    }
    Label{
        font-family: 'Poppins', sans-serif;

    }
    .high1{
        text-align: left;
        margin-bottom: 20px;
        text-decoration-style: solid;
    }
    button{
        border: none;
    }

    .updatebtn .class{

        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 30px;
        padding-right: 30px;
        border-radius: 5px;

        margin-top: 50px;
        background-color: #a8b3fc;
        cursor: pointer;
        transition: 0.4s;
        font-family: 'Poppins', sans-serif;
        font-weight: 600;
        font-size: 13px;

    }
    .updatebtn .class:hover{
        background-color:  #8191ff;
        color: white;
    }
    .goback{
        margin-left: 20px;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 25px;
        padding-right: 25px;
        border-radius: 5px;

        margin-top: 50px;
        background-color: #fca8d8;
        cursor: pointer;
        transition: 0.4s;
        font-family: 'Poppins', sans-serif;
        font-weight: 600;
        font-size: 13px;
        color: black;
        text-decoration: none;

    }
    .goback:hover{
        background-color: #fd72c7;
        color: white;
    }
    .ToBackButton{
        display: none;
        width:100%;
        max-height: 40px;
    }
    .back{
        padding: 5px;
        background: #959dff;
        border-radius: 50%;
        box-shadow: 0 3px 5px rgba(0,0,0,0.3);
        border: none;
        cursor: pointer;
        z-index: 99;
        transition: .4s;
    }
    .back:hover{
        background: #5967fd;
    }
    i.icon-back {
        margin:5px;
        border-radius: 50%;
        color: #ffffff;
    }
    i.icon-back:hover {
        border-radius: 50%;
        color: #ffffff;
    }



</style>
<body>

<header class="primary-header">
    <div class="hero">

        <nav>
            <?php
            foreach ($logox as $product){?>
            <?php echo
            $this->Html->image('/webroot/'.$product->image,
                array('class'=>'logo',
                    'url' => array('controller' => 'home', 'action' => 'dashboardPage'
                    )));
            ?>
            <?php
            }
            ?>
            <ul class="nav-menu">
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Dashboard</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['controller'=>'home','action' => 'dashboardPage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Mail Inbox</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['controller'=>'home','action' => 'contactmsgpage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><?php echo $this->Html->link('<span class="ITEMX1">Settings</span><span class="sr-onlyxx">'
                        . __('') .
                        '</span>',['controller'=>'home','action' => 'settingspage'],
                        ['escape' => false, 'title' => __('')]) ?></li>
                <li class="nav-item"><a href="#"></a></li>
                <?php
                foreach ($footerx as $footer){?>
                <div class="hamburger-footer">
                    <li class="nav-itemout"> <?php echo $this->Html->link('<span class="ITEMX1">Log Out</span><span class="sr-onlyxx">'
                            . __('') .
                            '</span>',['action' => 'logout'],
                            ['escape' => false, 'title' => __('')]) ?> </li>
                    <li class="nav-itemp"><p>ABN: <?=$footer->abn?></p</li>
                    <li class="nav-itemp"><p>ACR: <?=$footer->acr?> | AFCA: <?=$footer->afca?></p></li>
                    <li class="nav-itemp"><p>ACL: <?=$footer->acl?> | MFAA: <?=$footer->mfaa?></p></li>
                    <li class="nav-itemx"><p ><?=$footer->copyright?></p></li>

                </div>
                    <?php
                }
                ?>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>
</header>
        <div class="main-area">
            <div class="content">
                <div id="List_Group_Desktop_kn" class="List_Group_Desktop">

                    <div class = "ToBackButton">
                        <button class="back" title="Go Back" >
                            <?php echo $this->Html->link('<span class="fa fa-search"></span><span class="bac-k">'
                                . __('<i class="material-icons icon-back">arrow_back</i>') .
                                '</span>',["action"=>"contactmsgpage"],
                                ['escape' => false, 'title' => __('Go Back'),"class"=>"btn btn-warning"]) ?>
                        </button>
                    </div>

                    <div class="edit-contact-detail">

                        <div class="high1">
                            <h1>Update User</h1>
                        </div>

                        <?= $this->Form->create($user) ?>


                        <?php
                        echo $this->Form->control('email',["maxlength"=>"34",
                            'required' => true
                        ]);
                        echo $this->Form->control('password',["maxlength"=>"250",
                            'required' => true
                        ]);
                        ?>

                        <div class="updatebtn">

                            <?php echo $this->Form->button("Save",["class","btn btn-warning"]);
                            echo $this->Form->end()?>
                            <?= $this->Html->link('Go Back',
                                ['controller' => 'users', 'action' => 'index'],['class'=>"goback"]); ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<footer>
    <div class="primary-footer" id="page-bottom">
        <div class="footer-layout">
            <div class="footer-margin">
                <hr>
                <?php
                foreach ($footerx as $footer){?>

                    <div class="footer-items">
                        <div class="left-section">
                            <p class="copyright2"><?=$footer->copyright?></p>
                        </div>
                        <div class="center-section">

                        </div>
                        <div class="right-section">
                            <p class="security-no">ABN: <?=$footer->abn?> | ACR: <?=$footer->acr?> | ACL: <?=$footer->acl?> | AFCA: <?=$footer->afca?> | MFAA: <?=$footer->mfaa?></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</footer>



</body>
<script type="text/javascript">
    const hamburger = document.querySelector(".hamburger");
    const navMenu = document.querySelector(".nav-menu");

    hamburger.addEventListener("click",()=>{
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
    })
</script>
</html>
