<h1>Supplier Info</h1>
<?= $this->Html->link("Add",["action"=>"add"]) ?>


<?=$this->Form->create(null,["type"=>"get"]) ?>
<?=$this->Form->control("key",["label"=>"Search","value"=>$this->request->getQuery("key")]) ?>
<?=$this->Form->submit() ?>
<?=$this->Form->end() ?>

<table>
    <thead>
    <tr>

        <th>ContactName</th>
        <td></td>
        <td></td>
        <td></td>
        <th>CompanyName</th>

        <th>PhoneNo</th>

        <th>Email</th>

        <th>Address</th>



    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($suppliers as $supplier_info){?>

        <tr>


            <td>
                <?=$supplier_info->ContactName ?>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <?=$supplier_info->CompanyName ?>
            </td>
            <td>
                <?=$supplier_info->PhoneNo ?>
            </td>
            <td>

                <?=$supplier_info->Email?>

            </td>
            <td>
                <?=$supplier_info->Address ?>
            </td>


            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>

            <td>
                <?php echo $this->Html->link("Edit",["action"=>"edit",$supplier_info->SupplierID],["class"=>"btn btn-warning"]) ?>
            </td>
            <td>
                <?php echo $this->Form->postLink("Delete",["action"=>"delete",$supplier_info->SupplierID],["class"=>"btn btn-danger","confirm"=>"Are you sure?"]) ?>
            </td>


        </tr>





        <?php


    }
    ?>
    </tbody>
</table>

