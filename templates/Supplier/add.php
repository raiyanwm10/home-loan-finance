<h1>Add Supplier Info</h1>

<?php

echo $this->Form->create($suppliers);

echo $this->Form->control("SupplierID",["type"=>"hidden","value"=>""]);
echo $this->Form->control("ContactName");
echo $this->Form->control("CompanyName");
echo $this->Form->control("Email");
echo $this->Form->control("PhoneNo");
echo $this->Form->control("Address",["rows"=>"2"]);

echo $this->Form->button("Save");
echo $this->Html->link("Back",["action"=>"index"]);

echo $this->Form->end();


?>
